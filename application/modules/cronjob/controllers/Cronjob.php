<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Cronjob extends MX_Controller {


	public function index()
	{
		echo 'cronjob';
	}


	function sms($resend = false) {

		$hourStart = 9;
		$hourEnd = 15;
		$start = strtotime('2016-08-01 12:00');

		$hour = (int) date('H');
		if ($hour < $hourStart OR $hour > $hourEnd) {
			die('He thong SMS chi hoat dong tu ' . $hourStart . '-' . $hourEnd . 'h');
		}

		if (time() < $start) {
			die('SMS se duoc gui tu ' . date('H:i d/m/Y', $start));
		}

		$this->load->model('Messages');
		/*
		Bình thường sẽ gửi tất cả các SMS chưa được gọi hàm sendSMS [field sent]
		Nếu có tham số resend thì sẽ gửi lại tất cả các SMS chưa thành công [field status]
		*/
		$whereField = $resend ? 'status' : 'sent';
		$messages = $this->db->query('SELECT * FROM `sms` WHERE `' . $whereField . '` = 0 AND (`schedule` <= NOW() OR `schedule` IS NULL) LIMIT 10')->result();
		foreach ($messages as $message) {

			$content = $message->content;

			if ($this->input->ip_address() !== '127.0.0.1') {
				$res = $this->Messages->send($message->phone, $content);
			} else {
				$res['status'] = 1;
			}

			$updateData = [
				'sent' => 1,
				'status' => ($res['status'] == 1 ? 1 : 0)
			];
			if ($res['status'] == 1) {
				$updateData['delivered_at'] = date('Y-m-d H:i:s');
			}

			$this->db->where('id', $message->id)->update('sms', $updateData);

			echo '[' . strlen($content) . ' ky tu] => ' . $content . ($res['status'] == 1 ? ' [OK]' : ' [Fail]') . '<br>';
		}

		$remain = $this->db->where('sent', 0)->count_all_results('sms');
		if ($remain > 0) {
			echo 'Con lai ' . $remain . ' SMS chua duoc gui.';
		} else {
			$failSMS = $this->db->where('status', 0)->count_all_results('sms');
			if ($failSMS === 0) {
				$balance = $this->Messages->getBalance();
				echo 'Da gui thanh cong tat ca cac SMS';
				echo '<p>So tien con trong tai khoan la ' . number_format($balance) . ' vnd</p>';

				if ($balance < 20000) {
					//$sms->send('0974658440', '[TownDeli.VN] So tien con lai trong tai khoan he thong SMS la ' . number_format($balance) . ' vnd');
				}
			} else {
				echo 'Con lai ' . $failSMS . ' SMS bi loi. Hay bam vao link sau de gui lai <a href="' . site_url('cronjob/sms/resend') . '">Resend</a>';
			}			
		}
	}
}
