<?php echo $home_slider; ?>

    <div class="profile-page container-fluid" id="proFile">
        <div class="container">
            <div class="row">
                <div class="avata col-md-3">
                    <div class="wrap">
                        <a href="#"><img src="<?php echo asset_url('images/author.jpg') ?>" /></a>
                        <div class="mask">
                            <a href="">EDIT AVATA</a>
                        </div>
                    </div>
<!--
                    <div class="name-level">
                        <p class="score active"><img src="images/icon1.png"><br>Điểm tích lũy: 9<br><span>Cấp độ1:<br> New Townie</span></p>
                        <p class="score"><img src="images/icon2.png"><br>Điểm tích lũy: 99<br><span>Cấp độ2:<br> Silver Townie</span></p>
                        <p class="score"><img src="images/icon3.png"><br>Điểm tích lũy: 999<br><span>Cấp độ3:<br>Gold Townie</span></p>
                        <p class="score"><img src="images/icon4.png"><br>Điểm tích lũy: 9999<br><span>Cấp độ4:<br> Diamond Townie</span></p>
                    </div>
-->
                </div>
                <div class="tex-content col-md-9">
                    <form class="form-contact">
                        <h4> MEMBER NAME <strong>MIKHAIL TUAN DUONG</strong><i class="fa fa-pencil-square-o"></i></h4>
                        <div class="topContact">
                            <p>
                                <input type="text" class="form-control">
                                <i class="fa fa-pencil-square-o"></i>
                            </p>
                            <p>
                                <span>NEW PASSWORD </span>
                                <input type="password" name="password" class="form-control">
                            </p>
                            <p>
                                <span>CONFIRM<br>OLD PASSWORD</span>
                                <input type="password" name="password" class="form-control">
                            </p>
                        </div>
                        <div class="bottomContact">
                            <p>
                                <span>ADDRESS 1</span>
                                <input type="text" class="form-control">
                                <i class="fa fa-pencil-square-o"></i>
                            </p>
                            <p>
                                <span>ADDRESS 2</span>
                                <input type="text" class="form-control">
                                <i class="fa fa-pencil-square-o"></i></p>
                            <p>
                                <span>BILLING INFO</span>
                                <textarea></textarea>
                                <i class="fa fa-pencil-square-o"></i>
                            </p>
                        </div>
                        <button>SAVE</button>
                    </form>
                    <div class="history-order">
                        <h4>ORDER HISTORY & REWARD</h4> <p class="title">  REWARD POINT: 1366 </p>
                        <ul>
                            <li class="col-history">
                                    <h5>DATE</h5>
                                    <p>JAN 01</p>
                                    <p>JAN 01</p>
                                    <p>JAN 01</p>
                                    <p>JAN 01</p>
                                    <p>JAN 01</p>
                            </li>
                            <li class="col-history">
                                    <h5>ORDER</h5>
                                    <p>Espresso, Latte...</p>
                                    <p>Espresso, Latte...</p>
                                    <p>Espresso, Latte...</p>
                                    <p>Espresso, Latte...</p>
                                    <p>Espresso, Latte...</p>
                            </li>
                            <li class="col-history">
                                    <h5>ORDER ID</h5>
                                    <p>1501010006</p>
                                    <p>1501010006</p>
                                    <p>1501010006</p>
                                    <p>1501010006</p>
                                    <p>1501010006</p>
                            </li>
                            <li class="col-history">
                                    <h5>POINTS</h5>
                                    <p>136</p>
                                    <p>136</p>
                                    <p>136</p>
                                    <p>136</p>
                                    <p>136</p>
                            </li>
                        </ul>	
                    </div>
                </div>

            </div>

        </div>
    </div>