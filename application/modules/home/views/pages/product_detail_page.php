<?php 

$menu_sizes = [
    12 => 'price',
    16 => 'price_m',
    22 => 'price_l'
];

?>
<div class="wrap" data-id="<?php echo $row->id ?>">
    <p>
        <img src="<?php echo asset_url('images/coffe.png') ?>" alt="">
    </p>
    <div class="right-popup">
        <div class="top">
            <h5><?php echo $row->name ?></h5>
                <p class="icon">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                </p>
            <p class="text"><?php echo $row->short_description ?></p>
        </div>
        <div class="bottom">
            <div class="topBottom">
                <p class="menu_size">
                <?php foreach ($menu_sizes as $size => $priceField) {
                    $price = (int) $row->{$priceField};
                    if ($price > 0) {
                    ?>
                    <a data-field="<?php echo $priceField ?>" data-price="<?php echo $price ?>" <?php if ($posts['size'] == $size) echo 'class="selected"' ?>><?php echo $size ?>oz</a>
                <?php }} ?>
                </p>
                <h6>QUANTITY <input id="menu_qty" type="number" value="1" style="display:inline-block;width:100px;text-align: center" min="1"></h6>
            </div>
            <div class="form-qr <?php if (empty($toppings)) echo 'hidden' ?>">
               <div class="col-md-3 col-xs-12">
                    Chọn topping: <select id="pick_topping" class="form-control select2">
                        <option value="">Vui lòng chọn</option>
                        <?php foreach ($toppings as $topping): ?>
                        <option value="<?php echo $topping->id ?>" data-price="<?php echo $topping->price ?>"><?php echo $topping->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="end toppingSelecteds">
                </div>
            </div>
            <div class="plus">
               <i class="fa fa-heart-o"></i><i class="fa fa-heart"></i>
                <a class="button">
                    <span class="cart_total"><?php echo number_format($row->{$menu_sizes[$posts['size']]}) ?> đ</span>
                    <div class="wrap">
                        <i class="glyphicon glyphicon-plus"></i>
                        <i class="fa fa-check"></i>
                    </div>
                </a>
            </div>
            <span class="exit"><i class="glyphicon glyphicon-remove"></i></span>
        </div>
</div>