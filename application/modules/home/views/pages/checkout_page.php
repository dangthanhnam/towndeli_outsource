<?php 

$cartSubTotal = 0;
$cartTotal = 0;

 ?>
<!-- main content -->
<div class="home-page checkOut">

	<?php echo $home_slider; ?>

    <div class="container">
        <div class="row checkCart cartCheckout">
            <div class="topCart">
                <h2 class="title-h2">Đơn Hàng <span>Drink more</span></h2>
                <?php if (!empty($cart_items)): ?>
                    <table class="table myCart">
                        <tbody>
                        <?php foreach ($cart_items as $rowID => $row): ?>
                            <tr data-id="<?php echo $rowID; ?>" data-price="<?php echo $row['price'] ?>">
                                <td style="width: 20%">
                                    <a class="btn btn-xs cart_btn_remove"><span class="glyphicon glyphicon-remove"></span></a><?php echo $row['name'] ?> (<?php echo $this->Menus->getSizeOz(@$row['options']['size']) ?>)
                                </td>
                                <td style="width: 40%">
                                    <?php echo getCartContentToppings(@$row['options']['toppings']); ?>
                                </td>
                                <td style="width: 20%" class="cart_quantity">
                                    <div><i class="glyphicon glyphicon-plus"></i><span class="cart_item_qty"><?php echo $row['qty'] ?></span><i class="glyphicon glyphicon-minus"></i></div>
                                </td>
                                <td style="width: 10%" class="cart_edit">
                                    <span><i class="fa fa-pencil-square-o"></i></span>
                                </td>
                                <td style="width: 10%">
                                    <span class="cart_subtotal"><?php echo number_format($row['subtotal']) ?></span>
                                </td>
                            </tr>
                            <?php $cartSubTotal += $row['subtotal']; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>

                <div class="voucherCart cartDiscount">
                    <p>
                        <label for="">MÃ KHUYẾN MÃI</label>
                        <input type="text" class="form-control" name="discount_code" placeholder="Nhập mã khuyến mãi" />
                    </p>
                    <span>TẠM TÍNH: <span class="cartSubTotal"><?php echo number_format($cartSubTotal) ?></span> VNĐ</span>
                </div>
                <div class="serviceCart">
                    <span class="total">TỔNG CỘNG: <span class="cartTotal"><?php echo number_format($cartSubTotal) ?></span> VNĐ</span>
                    <span>
                        <a class="next btnCheckout">Bước tiếp</a>
                    </span>
                </div>
            </div>
            <!--End TopCart-->
            <div class="customerInfo">
                <h2 class="title-h2">Thông tin khách hàng <i class="fa fa-question-circle "></i></h2>
                <form action="" method="">
                    <div class="topForm">
                        <div class="formLeft">
                            <p>
                                <label for="">Họ tên</label> 
                                <input type="text" class="form-control" name="customer_name" placeholder="Họ tên khách hàng" required="" />
                            </p>
                            <p class="email">
                                <label for="">EMAIL</label>
                                <input type="text" class="form-control" name="customer_email" placeholder="Email khách hàng" />
                            </p>
                            <p class="mobile">
                                <label for="">Số ĐT</label>
                                <input type="text" class="form-control" name="customer_phone" placeholder="Số điện thoại khách hàng" required="" />
                            </p>
                        </div>
                        <div class="formRight">
                            <div class="left">
                                <span>
                                    <label for=""><i class="fa fa-check"></i></label>
                                    <input type="checkbox" class="form-control" class="cbox_gift" />
                                </span>
                                <label for="">Tặng món quà này cho...</label>
                            </div>
                            <div class="right">
                                <div class="name showForm">
                                    <label for="">Họ tên</label>
                                    <input type="text" class="form-control" name="receiver_name" placeholder="Họ tên người nhận">
                                </div>
                                <div class="mobile showForm">
                                    <label for="">Số ĐT</label>
                                    <input type="text" class="form-control" name="receiver_phone" placeholder="Số điện thoại người nhận" />
                                </div>
                                <div class="message showForm">
                                    <label for="">Lời nhắn <span>(không bắt buộc)</span></label>
                                    <textarea rows="4" name="receiver_message" placeholder="Quý khách có điều chi nhắn gửi? Vui lòng điền vào đây"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottomForm">
                        <div class="local">
                            <label for="">Giao đến</label>
<!--                             <select id="selectLocal" class="select2">
                                <option>Chọn địa điểm</option>
                                <option>Option 1</option>
                                <option>Option 2</option>
                            </select> -->
                        </div>
                        <div class="address">
                            <div class="input-group">
                                  <input type="text" class="form-control" name="shipping_address" placeholder="01 Le Duan, Da Kao, Quan 1, HCMC, Vietnam" aria-describedby="basic-addon2" required="" />
                                  <span class="input-group-addon" id="basic-addon2">
                                      <i class="glyphicon glyphicon-map-marker"></i>
                                  </span>
                            </div>
                        </div>
                        <span>
                            <a class="next btnCheckout">Bước tiếp</a>
                        </span>
                    </div>
                </form>
            </div>
            <!--End Info-->
            <div class="scheduleMore">
                <h2 class="title-h2">schedule <span>Schedule more</span></h2>
                <ul>
                    <li><i class="close-cart">+</i></li>
                    <li><span>09:00 AM. 01/01, 05/01, 06/01, 07/01, 12/01, 20/01, 21/01</span></li>
                    <li><i class="fa fa-pencil-square-o"></i></li>
                </ul>
                <span>
                    <a class="next btnCheckout">Bước tiếp</a>
                </span>
            </div>
            <!--End schedule-->
            <div class="payName">
                <h2 class="title-h2">Thanh toán <i class="fa fa-question-circle "></i></h2>
                <div class="selectPay">
                    <label for="">Thanh toán bằng</label>
                    <select  id="selectName" class="select2">
                        <option value="">Tiền mặt</option>
                    </select>
                </div>
                <div class="addPay">
                    <label for="">Ghi chú</label>
                    <textarea name="note" placeholder="Quý khách muốn nhận càng sớm càng tốt hay tại thời điểm khác? Quý khách sẽ thanh toán bằng số tiền bao nhiêu? Quý khách có yêu cầu, ghi chú nào khác?"></textarea>
                </div>
                <span><a class="next btnCheckout">Bước tiếp</a></span>
            </div>
        </div>
    </div>


</div>
<!-- end content -->