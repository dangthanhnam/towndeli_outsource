    <div class="slider">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="<?php echo asset_url('images/home-slider-1.png') ?>" alt="" />
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="info">
                                <h1>COMBO 4 LY TỰ CHỌN GIÁ 99K</h1>
                                <p>Combo hấp dẫn đánh tan cơn khát ngày hè nóng bức!!!<br>Tự chọn 4 ly size 16oz bất kỳ trong Menu của TownDeli<br>với giá chỉ 99k & được giao tận nơi Miễn Phí.</p>
                                <a class="btn-banner" href="#">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-slider"></div>
    </div>