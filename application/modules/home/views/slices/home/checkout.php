	<!-- checkout block -->
	<div class="checkout-block">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6 left-img">
					<img src="<?php echo asset_url('images/how-to.png') ?>" style="height:320px" alt="">
				</div>
				<div class="col-xs-12 col-md-6">
					<h2 class="block-title">HOẠT ĐỘNG RA SAO??</h2>
					<ul class="tour-works">
						<li class="pick">
							<p>1. CHỌN MÓN YÊU THÍCH</p>
							<p>Với Menu đặc sắc hơn 60 món của TownDeli, bạn có thể gọi đặt món qua tổng đài (08) 73073777 hoặc Order Online.</p>
						</li>
						<li class="date">
							<p>2. CUNG CẤP THÔNG TIN GIAO HÀNG</p>
							<p>Bạn có thể yêu cầu TownDeli giao vào thời điểm thích hợp hoặc giao tận phòng làm việc. Thậm chí, bạn có thể chọn gửi tặng cho người khác nữa!</p>
						</li>
						<li class="enjoy">
							<p>3. GIAO TẬN NƠI MIỄN PHÍ CHỈ SAU 20-40 PHÚT</p>
							<p>Hiện tại TownDeli triển khai giao tận nơi tại khu vực Quận 1, 3, 10, Phú Nhuận, Tân Bình và Bình Thạnh.</p>
						</li>
					</ul>	
				</div>
			</div>
		</div>
		<div class="pick-block container-fluid hidden">
			<form action="#" method="POST" role="form">
				<div class="container">
					<div class="row" id="calendar">
						<div class="col-xs-12"><h2 class="block-title">1.PICK A COFFEE</h2></div>
						<div class="col-xs-6 col-sm-3">
							<label for='pick_coffee'>COFFEE</label>
							<select name="pick_coffee" id="pick_coffee" class="form-control select2">
								<option value="ESPRESSO">ESPRESSO</option>
								<option value="ESPRESSO">ESPRESSO</option>
							</select>
						</div>
						<div class="col-xs-6 col-sm-3">
							<label for='pick_size'>SIZE</label>
							<select name="pick_size" id="pick_size" class="form-control select2">
								<option value="ESPRESSO">MEDIUM</option>
								<option value="ESPRESSO">MEDIUM</option>
							</select>
						</div>
						<div class="col-xs-6 col-sm-3">
							<label for='pick_topping'>TOPPING</label>
							<select name="pick_topping" id="pick_topping" class="form-control select2">
								<option value="ESPRESSO">COOKIES</option>
								<option value="ESPRESSO">COOKIES</option>
							</select>
						</div>
						<div class="col-xs-6 col-sm-3">
							<label for='pick_quanity'>QUANTITY</label>
							<select name="pick_quanity" id="pick_quanity" class="form-control select2">
							<?php for ($i = 1; $i <= 10; $i++): ?>
								<option value="<?php echo $i ?>"><?php echo $i ?></option>
							<?php endfor; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12"><h2 class="block-title">2.DELIVERY DATES</h2></div>
						<div class="col-xs-6">
							<label for='pick_date'>DATE</label><br>
							<div class="input-group date-picker"> 
								<input id="datepicker" type="text" class="form-control" aria-label="Text input with multiple buttons"  placeholder="09/03/2016" />
								<div class="input-group-btn"> 
									<button type="button" class="btn btn-default" aria-label="Help" >
										<i class="fa fa-calendar"></i>
									</button>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<label for='pick_time'>TIME</label>
							<select name="pick_time" id="pick_time" class="form-control select2">
								<option value="ESPRESSO">09:30:00</option>
								<option value="ESPRESSO">09:30:00</option>
							</select>
						</div>
						<div class="col-md-3 col-xs-12 ask_delitm">
							<label for='ask_delitm'>new</label>
							<div class="ask_delitm">
								<label>
									I WANT<br/><strong>DESK DELITM *</strong>
									<div class="delitm-checkbox">
									    <input name='ask_delitm' type="checkbox" value="1" checked>
									    <label for="delitm"><span></span></label>
									</div>
								</label>
							</div>
						</div>	
					</div>
					<div class="for-detail row">
						<div class="col-md-9 col-ms-9">
							<h5>*<strong>DESK DELITM</strong> DELIVERY</h5>
							<p>Delivery at your desk may take extra cost, please check our current <a href="#">Delivery Pricing</a></p>
						</div>
						<div class="col-xs-3">
							<button type="submit" class="btn"><span>CHECKOUT</span><i class="fa fa-angle-right"></i></button>
						</div>
					</div>
					
				</div>
				
			</form>
		</div>
	</div>