	<!-- product block -->
	<div class="product-block" id="productBlock">
		<div class="container">
			<?php if (!empty($comboMenus)): ?>
			<h2 class="block-title">hot trends</h2>
			<!-- list-combo -->
			<div class="list-products row">
				<div class="col-xs-12">
					<h3 class="section-title">Combo</h3>
				</div>
				<?php foreach ($comboMenus as $menu): ?>
				<div class="product col-xs-6 col-md-6 col-ms-6">
					<div class="main-img"><img src="<?php echo asset_url('images/product.png') ?>" alt=""></div>
					<div class="content">
						<div class="detail">
							<h6 class="title"><?php echo $menu->name ?></h6>
							<p class="price"><?php echo number_format($menu->price) ?></p>
							<div class="desc"><?php echo $menu->short_description ?></div>
							<span class="add-cart btn-product">ADD</span>
							<div class="option">
								<p class="standard">
									<span class="btn-product">standard</span>
								</p>
								<p class="extra">
									<label>+<?php echo number_format($menu->price_m) ?> <span>đ</span></label>
									<span class="btn-product">extra</span>
								</p>
								<p class="extra-big">
									<label>+<?php echo number_format($menu->price_l) ?> <span>đ</span></label>
									<span class="btn-product">extra big</span>
								</p>
							</div>
						</div>
					</div>					
				</div>
				<?php endforeach; ?>
			</div><!-- end combo -->
			<?php endif; ?>

			<?php foreach ($groupMenus as $group): ?>
<!-- BEGIN: list [<?php echo $group['category']->name ?>] -->
			<div class="list-products row">
				<div class="col-xs-12">
					<h3 class="section-title"><?php echo $group['category']->name ?></h3>
					<?php if (!isset($group['only'])): ?>
					<a class="load-more" href="<?php echo module_url('category/' . $group['category']->slug) ?>">xem thêm</a>
				<?php endif; ?>
				</div>

				<?php 
					$menu_sizes = [
						'price' => [12, 'standard'],
						'price_m' => [16, 'extra'],
						'price_l' => [22, 'extra big']
					];
				 ?>

				<?php foreach ($group['menus'] as $menu): ?>
				<?php 
					//$menu_img = asset_url('upload/menus/' .$menu->id );
				 ?>
				<div class="product col-xs-6 col-md-3 col-ms-6" data-id="<?php echo $menu->id ?>">
					<div class="main-img"><img src="<?php echo asset_url('images/coffe.png') ?>" alt=""></div>
					<div class="content">
						<div class="detail">
							<h6 class="title"><?php echo $menu->name ?></h6>
							<p class="price"><?php echo number_format($menu->price) ?> đ</p>
							<div class="desc"><?php echo $menu->short_description ?></div>
							<span class="add-cart btn-product">Thêm</span>

							<div class="option">
								<?php foreach ($menu_sizes as $key => $value) {
									$price = (int) $menu->{$key};
									if ($price > 0) {
								?>
								<p class="<?php echo slug($value[1]) ?>" data-price="<?php echo $price ?>" data-size="<?php echo $value[0] ?>">
									<span class="btn-product"><?php echo $value[1] ?></span>
								</p>
								<?php }} ?>
							</div>
						</div>						
						
					</div>
				</div>
				<?php endforeach; ?>
			</div>
			<!-- END: [<?php echo $group['category']->name ?>] -->
			<?php endforeach; ?>

		</div>
	</div><!-- END Product -->