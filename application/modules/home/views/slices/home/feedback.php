	<!-- feedback block -->
	<div class="feedback-block">
		<div class="container">
			<div class="list-feedback row">
				<div class="item">
					<div class="author-img col-xs-2">
						<img src="/assets/images/author.jpg" alt="author">
					</div>
					<div class="col-xs-10">
						<div class="content">
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
						</div>
						<span class="author">Mikhail Tuan Duong, Freelance Creative</span>
						<div class="rating">
							<span class="star"></span>
							<span class="star"></span>
							<span class="star"></span>
							<span class="star"></span>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="author-img col-xs-2">
						<img src="/assets/images/author.jpg" alt="author">
					</div>
					<div class="col-xs-10">
						<div class="content">
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
						</div>
						<span class="author">Mikhail Tuan Duong, Freelance Creative</span>
						<div class="rating">
							<span class="star"></span>
							<span class="star"></span>
							<span class="star"></span>
							<span class="star"></span>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="author-img col-xs-2">
						<img src="/assets/images/author.jpg" alt="author">
					</div>
					<div class="col-xs-10">
						<div class="content">
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
						</div>
						<span class="author">Mikhail Tuan Duong, Freelance Creative</span>
						<div class="rating">
							<span class="star"></span>
							<span class="star"></span>
							<span class="star"></span>
							<span class="star"></span>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div><!-- END feedback -->