	<!-- footer -->
	<div id="footer">
		<div class="footer-top">
			<div class="app-footer">
				<div class="app-footer-left col-md-3 col-xs-4">
					<img src="/assets/images/images-mb.png">
				</div>
				<div class="app-footer-right col-md-9 col-xs-8">
					<p>Pay with you phone. And do so much more</p>
					<a href="#"><img src="<?php echo asset_url('images/app-store.png') ?>"></a>
					<a href="#"><img src="<?php echo asset_url('images/ggplay.png') ?>"></a>
				</div>
			</div>
			<div class="link-social">
				<ul class="slink-social-left col-md-6">
					<li><a class="facebook" href="https://www.facebook.com/towndeli/" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a class="twitter hidden" href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a class="google hidden" href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a class="instagram" href="https://www.instagram.com/towndelivietnam/" target="_blank"><i class="fa fa-instagram"></i></a></li>
					
				</ul>
				<div class="link-social-right col-md-6">
					<p>BẠN ĐÃ SẴN SÀNG LÀ MỘT TOWNIE?</p>
					<input type="text" class="form-control" placeholder="Email của bạn">
					<button class="btn btn-default" type="button">GIA NHẬP</button>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-about">
						<h4 class="title-ft">VỀ TOWNDELI</h4>
						<ul class='contact-info'>
							<li>Dịch Vụ Đồ Uống Giao Tận Nơi Số 1 Việt Nam</li>
							<li>Hotline: (08) 73073777</li>
							<li>Email: hi@towndeli.vn</li>
							<li><a href="">Câu Hỏi Thường Gặp</a></li>
							<li><a href="">Cần Hỗ Trợ Thêm?</a></li>
							<li><a href="">Hợp Tác</a></li>
						</ul>
						<ul class="social">
							<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
							<li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
						</ul>
					</div>
					<div class="col-xs-12 col-blog">
						<h4 class="title-ft">KHÁM PHÁ</h4>
						<ul class="news-ft">
							<li>
								<p><a href="#">Transitions In UX Design</a></p>
								<span>October 4,2003</span>
							</li>
							<li>
								<p><a href="#">Transitions In UX Design</a></p>
								<span>October 4,2003</span>
							</li>
							<li>
								<p><a href="#">Transitions In UX Design</a></p>
								<span>October 4,2003</span>
							</li>
						</ul>
					</div>
					<div class="col-xs-12 col-link">
						<h4 class="title-ft">GIẤY PHÉP</h4>
						<ul class="further-link">
							<li>Công ty TNHH Town Vietnam</li>
							<li>Giấy phép ĐKKD : 0313522773 do Sở KH&amp;ĐT TP.HCM cấp ngày 05/11/2015</li>
						</ul>
					</div>
					<div class="col-xs-12 col-photo">
						<h4 class="title-ft">INSTAGRAM</h4>
						<div class="photo-stream">
							<a href="<?php echo asset_url('images/photo.png') ?>"><img src="<?php echo asset_url('images/photo.png') ?>" alt=""></a>
							<a href="<?php echo asset_url('images/photo.png') ?>"><img src="<?php echo asset_url('images/photo.png') ?>" alt=""></a>
							<a href="<?php echo asset_url('images/photo.png') ?>"><img src="<?php echo asset_url('images/photo.png') ?>" alt=""></a>
							<a href="<?php echo asset_url('images/photo.png') ?>"><img src="<?php echo asset_url('images/photo.png') ?>" alt=""></a>
							<a href="<?php echo asset_url('images/photo.png') ?>"><img src="<?php echo asset_url('images/photo.png') ?>" alt=""></a>
							<a href="<?php echo asset_url('images/photo.png') ?>"><img src="<?php echo asset_url('images/photo.png') ?>" alt=""></a>
							<p><a href="#">View stream on flicker</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="wrapSub hidden">
	    <div class="menu-subs popup">
	        <div class="detail">
	            <img src="/assets/images/sale-off.png" alt="sale">
	            <a>Subscribe and get 40% off !</a>
	            <a>SUBSCRIBE NOW !</a>
	        </div>
	        <a><i class="fa fa-times"></i></a>
	    </div>	
	</div>
	<div id="bg_wrap"></div>

	<script>
		var PARAMS = {
			siteURL: "<?php echo site_url() ?>",
			ajaxURL: "<?php echo module_url('ajax') ?>",
			moduleURL: "<?php echo module_url() ?>",
			model: "<?php echo $params->method ?>",
			method: "",
			action: "",
			uploadURL: ""
		};

		var API = {
			cart: "<?php echo site_url('api/cart') ?>",
			users: "<?php echo site_url('api/users') ?>",
			orders: "<?php echo site_url('api/orders') ?>",
		};

		var URL = {
			site: "<?php echo site_url() ?>",
			ajax: "<?php echo module_url('ajax') ?>",
			module: "<?php echo module_url() ?>"
		};
	</script>

    <script src="<?php echo asset_url('js/jquery.min.js') ?>"></script>  
    <script src="<?php echo asset_url('js/ie-emulation-modes-warning.js') ?>"></script>
    <script src="<?php echo asset_url('js/jquery.plugin.js') ?>"></script>
    <script src="<?php echo asset_url('bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo asset_url('js/ie10-viewport-bug-workaround.js') ?>"></script>
    <script src="<?php echo asset_url('owl.carousel/owl.carousel.min.js') ?>"></script>
    <script src="<?php echo asset_url('select2/select2.full.min.js') ?>"></script>
    <script src="<?php echo asset_url('js/jquery-ui.js') ?>"></script>
    <script src="<?php echo asset_url('global/scripts/common.min.js') ?>"></script>
    <script src="<?php echo asset_url('js/main.js') ?>"></script>