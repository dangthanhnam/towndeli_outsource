<?php 

$cartSubTotal = 0;

 ?>
<div id="cart-item"  class="popup" data-total-items="<?php echo $this->cart->total_items(); ?>">
    <div class="cat-top-info">
        <div class="title">
            <h5>Giỏ hàng</h5>
            <a href="<?php echo module_url('menu') ?>">Đặt thêm</a>
        </div>
        <?php if (!empty($cart_items)): ?>
        <div class="scrollbar" id="style-1">
            <ul>
                <?php foreach ($cart_items as $rowID => $row): ?>
                <li data-id="<?php echo $rowID; ?>" data-price="<?php echo $row['price'] ?>">
                    <p><i class="close-cart">+</i> <?php echo $row['name'] ?></p>
                    <p><?php echo getCartContentToppings(@$row['options']['toppings']); ?></p>
                    <p class="cart_quantity"><i class="glyphicon glyphicon-plus"></i> <span class="cart_item_qty"><?php echo $row['qty'] ?></span> <i class="glyphicon glyphicon-minus"></i></p>
                    <p><span class="cart_item_subtotal"><?php echo number_format($row['subtotal']) ?></span> VNĐ <i class="fa fa-pencil-square-o"></i> </p>
                </li>
                <?php $cartSubTotal += $row['price'] * $row['qty']; ?>
            <?php endforeach; ?>
            </ul>
            <div class="force-overflow"></div>
        </div>
        <?php endif; ?>
    </div>

    <div class="bottom-info">
            <p><span>Tổng cộng:<br><?php echo number_format($cartSubTotal) ?> VNĐ</span><br>Giao hàng miễn phí</p>
            <a href="<?php echo module_url('checkout') ?>" >Thanh toán <i class="glyphicon glyphicon-menu-right"></i></a>
    </div> 
    <a><i class="fa fa-times"></i></a>
</div>