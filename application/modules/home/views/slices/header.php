<!-- header -->
    <div id="header">
        <div class="header-top">
            <div class="container">
                <p><span>HOORAY!!! GIẢM THÊM 5%</span> CHO MỌI ĐƠN HÀNG KHI ĐẶT MÓN TRỰC TUYẾN TỪ HÔM NAY ĐẾN HẾT 30.06. ORDER NGAY <a>Xem chi tiết</a></p>
            </div>                        
        </div>        
        <div class="header-bottom">
        	<div id="header-menu">
                <div class="container">
                    <div class="categories-menu">
                        <i class="fa fa-bars"></i>
                        <div id="cat_popup" class="popup">
                            <h6 class="title">Danh mục</h6>
                            <ul class="hot-deals ul-hightlight">
                                <li><a>Khuyến mãi</a></li>
                                <li><a>Download Menu</a></li>
                                <li><a>Món ưa thích</a></li>
                            </ul>
                            <ul class="list-categories ul-right">
                                <?php foreach ($categories as $row): ?>
                                    <li><a href="<?php echo module_url('category/' . $row->slug) ?>"><?php echo $row->name ?><i class="fa fa-angle-right"></i></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div id="logo"><a href="/"><img src="<?php echo asset_url('images/logo.png') ?>" alt='logo'></a></div>
                    <ul class="main-menu">
                        <li class="login-menu">
                            <a href="#productBlock"><span>Thực đơn</span></a>
                        </li>
                        <li class="schedule">
                            <a href="#calende" onclick="alert('Chức năng này đang xây dựng');return false;"><span>Đặt Cafe sáng</span></a>	                        
                        </li>
                        <li class="cart">
                            <a><p>Đơn hàng</p><span class="count"><?php echo $this->cart->total_items(); ?></span></a>
                           
                        </li>
                    </ul>
                    <ul class="profile">
                        <li class="avatar">
                            <a href="">
                                <img src="<?php echo asset_url('images/avatar-default.png') ?>" alt='avartar'>
                            </a>
                           <div id="menu-schedule" class="popup">
                                <ul class="ul-hightlight">
                                    <li><a>Schedule</a></li>
                                    <li><a>Reward points</a></li>
                                </ul>
                                <ul class="pf-action ul-right">
                                    <li><a href="">Profile<i class="fa fa-angle-right"></i></a></li>
                                    <li><a>Log out<i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="lang">
                            <a><img src="<?php echo asset_url('images/flag-vi.png') ?>" alt=""></a>
                            <div id="menu-lang" class="popup hidden">
                                <ul class="pf-action ul-right">
                                    <li><a onclick="alert('Đang hoàn thiện chức năng này');return false;">English</a></li>
                                    <li><a>Tiếng Việt</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wrap-cart">
        <?php echo $cart_contents ?>
        </div>
        <!--End Cart-item-->
        <div class="bg-popup">     
            <div id="popup-home-page"></div>
            </div>
        </div>
        <!--End bg_popup-->
        <?php if ($this->session->userdata('userId') !== null): ?>
        <div class="wrap-login">
            <div id="form-login" class="popup form-login">
                <div class="top">
                    <h3 class="title" >Đăng nhập</h3>
                    <p>Cần tạo tài khoản? <a id="click-register">Đăng kí</a></p>
                </div>
                <div class="bottom">
                    <div class="social">
                        <a class='fb'><i class="fa fa-facebook"></i>Đăng nhập với facebook</a>                
                        <a class='gg'></a>
                        <a class='tw'><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="devide">
                        <hr />
                        <p>hoặc</p>
                    </div>
                    <form role="form" class="formUserSignIn">
                        <div class="form-group">
                            <input type="email" class="form-control" id="login-email" name="email" placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="login-password" name="password" placeholder="Mật khẩu" required="">
                        </div>
                        <div class="form-group">
                            <label for="remember">Ghi nhớ</label>
                            <div class="check-login">
                                <input type="checkbox" class="form-control" id="login-remember">
                                <label for="checkbox"><i class="fa fa-check"></i></label>
                            </div>
                            <a>Quên mật khẩu ?</a>
                        </div>
                        <button class="btn btn-primary btnUserSignIn">Đăng nhập</button>
                    </form>
                </div>
                <a><i class="fa fa-times-circle-o"></i></a>
            </div>
            <div id="form-register" class="popup form-login">
                <div class="top">
                    <h3 class="title" >Đăng kí</h3>
                    <p>Bạn đã có tài khoản ? <a id="click-login">Đăng nhập</a></p>
                </div>
                <div class="bottom">
                    <div class="social">
                        <a class="fb"><i class="fa fa-facebook"></i>Đăng nhập với facebook</a>                
                        <a class="gg"></a>
                        <a class="tw"><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="devide">
                        <hr />
                        <p>hoặc đăng kí tài khoản trên Towndeli</p>
                    </div>
                    <form role="form" class="formUserRegister">
                        <div class="top-register">
                            <div class="form-group">
                                <input type="text" class="form-control" id="register-name" name="firstname" placeholder="Tên" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="register-lastname" name="lastname" placeholder="Họ">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="register-phone" name="phone" placeholder="Số điện thoại" required="">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="register-email" name="email" placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="register-password" name="password" placeholder="Mật khẩu" required="">
                        </div>
                        <p>Bằng việc đăng kí sử dụng website, bạn đồng ý với các điều khoản quy chế</p>
                        <button class="btn btn-primary btnUserSignUp">Đăng kí</button>
                    </form>
                </div>
                <a><i class="fa fa-times-circle-o"></i></a>
            </div>  
        </div>
        <?php endif; ?>
        <!--End login-->
    </div>
    <!-- end header -->