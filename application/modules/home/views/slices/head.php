<title><?php echo $title ?> | TownDeli.VN</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content=""/>
	<meta name="author" content="Quốc Hưng Nguyễn"/>

	<?php echo chrome_frame(); ?>
	<?php echo view_port(); ?>
	<?php echo apple_mobile('black-translucent'); ?>
	<?php echo $meta; ?>

	<!-- icons and icons and icons and icons and icons and a tile -->
	<?php echo windows_tile(array('name' => 'Stencil', 'image' => base_url().'favicon.ico', 'color' => '#4eb4e5')); ?>
	<?php echo favicons(); ?>

	<!-- css -->
	<link rel="stylesheet" href="<?php echo asset_url('bootstrap/css/bootstrap.css') ?>" />
	<link rel="stylesheet" href="<?php echo asset_url('awesome/css/font-awesome.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo asset_url('owl.carousel/owl.carousel.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo asset_url('select2/select2.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo asset_url('css/style.css') ?>" />
	<link rel="stylesheet" href="<?php echo asset_url('css/resposive1.css') ?>" />
	<link rel="stylesheet" href="<?php echo asset_url('css/jquery-ui.css') ?>" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900" rel="stylesheet">
	<!-- end css-->

	<?php echo shiv(); ?>