<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Ajax extends MX_Controller {

    private $posts;


	function __construct()
	{
		parent::__construct();

        $this->posts = $this->input->post();

        $this->load->library('Cart');
        $this->cart->product_name_rules .= "\"\'\/\&";
        $this->load->model('Menus');
        $this->load->model('Toppings');
	}


	public function index()
	{
        $posts = $this->input->post();
        foreach (['useClass', 'useAction'] as $item) {
            ${$item} = isset($posts[$item]) ? $posts[$item] : '';
            unset($posts[$item]);
        }

        $data = $posts;
        $model = ucwords($useClass);
        $method = $useAction;
        $this->load->model($model);
        $this->$model->$method($data);
	}


    public function cart()
    {
        $contents = $this->cart->contents();
        $posts = $this->posts;

        if ($posts) {
            $action = isset($posts['action']) ? $posts['action'] : '';
            $rowID = isset($posts['rowid']) ? $posts['rowid'] : '';
            $options = $posts['options'];
            $toppings = isset($options['toppings']) ? $options['toppings'] : [];
            $qty = isset($posts['qty']) ? intval($posts['qty']) : 1;
            if ($qty < 1) {
                $qty = 1;
            }

            switch ($action) {

                case 'update':
                    if (isset($contents[$rowID])) {
                        $data = [
                            'rowid' => $rowID,
                            'qty' => $qty
                        ];

                        if (isset($posts['id'])) {
                            $currentData = $contents[$rowID];
                            $menu = $this->Menus->getDetail($posts['id']);
                            $currentData['price'] = (int) $menu->{$options['size']};

                            $data['price'] = $currentData['price'] + $this->_cartPrice($toppings);
                            $data['options'] = $currentData['options'];
                            $data['options']['size'] = $options['size'];
                            $data['options']['toppings'] = $toppings;
                        }

                        $this->cart->update($data);
                    }
                break;


                case 'remove':
                    $data = [
                        'rowid' => $rowID,
                        'qty' => 0
                    ];
                    $this->cart->update($data);
                break;


                default:
                    $menu = $this->Menus->getDetail($posts['id']);
                    $price = (int) $menu->{$options['size']};
                    $price += $this->_cartPrice($toppings);

                    if ($menu) {
                        $data = array(
                                'id'      => intval($menu->id),
                                'qty'     => $qty,
                                'price'   => $price,
                                'name'    => $menu->name,
                                'options' => $options
                        );
                        $this->cart->insert($data);
                    }
                break;
            }
        }
    }


    public function cart_discount()
    {
        if (isset($this->posts['discount_code'])) {
            $this->load->library('session');
            $this->load->model('Discounts');

            $code = $this->posts['discount_code'];
            $this->session->set_userdata('discount_code', $code);
            $res = $this->Discounts->check($code);
            if ($res > 0) {
                echo 1;
                die();
            }
        }

        echo 0;
    }


    private function _cartPrice($toppings)
    {
        $total = 0;
        foreach ($toppings as $toppingID => $toppingQty) {
            $topping = $this->Toppings->getDetail($toppingID);
            if ($topping) {
                $toppingQty = (int) $toppingQty;
                if ($toppingQty < 1) {
                    $toppingQty = 1;
                }
                $total += intval($topping->price) * $toppingQty;
            }
        }

        return $total;
    }


    public function menu()
    {
        $id = $this->input->post('id');
        $menu = $this->Menus->getDetail($id);

        if (is_null($menu)) {
            show_error('Menu is not found', 403);
        }

        $toppings = $this->Toppings->getList();

        $data = [
            'row' => $menu,
            'toppings' => $toppings,
            'posts' => $this->input->post()
        ];

        $this->load->view('pages/product_detail_page', $data);
    }


    public function get_cart_contents()
    {
        $data = [
            'cart_items' => $this->cart->contents()
        ];
        $this->load->view('slices/cart/contents', $data);
    }
}
