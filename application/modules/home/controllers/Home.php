<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends MX_Controller {

	private $segments = [];


	function __construct()
	{
		parent::__construct();

		$this->segments = array_slice($this->uri->rsegments, 2);

		$this->load->library('Template');

		$slices = [
            'cart/contents',
			'head',
			'header',
			'footer'
		];
		$this->template->slice($slices);

        $this->load->library('Cart');
        $this->cart->product_name_rules .= "\"\'\/\&";
        $this->load->model('Menus');
        $this->load->model('Toppings');
        $this->template->data('cart_items', $this->cart->contents());

		$this->load->model('Categories');
		$this->template->data('categories', $this->Categories->getAllActive());
	}


	public function index()
	{
		$slices = [
			'home/slider',
			'home/checkout',
			'home/product',
			'home/feedback'
		];
		$this->template->slice($slices);

		$this->template->title('Trang chủ');

		$this->load->model('Menus');
		$this->template->data('groupMenus', $this->Menus->getFeatures(3, 4));
		$this->template->data('comboMenus', $this->Menus->getCombos(2));

		$this->template->paint();
	}


    public function category($slug = '')
    {
    	$this->load->model('Categories');
    	$category = $this->Categories->getDetailByField('slug', $slug);
    	if (is_null($category)) {
    		redirect();
    	}

		$slices = [
			'home/slider',
			'home/product',
		];
		$this->template->slice($slices);

    	$this->template->title('Trang chủ');

    	$groupMenus = [];
    	$groupMenus[] = [
    		'category' => $category,
    		'menus' => $this->Menus->getList(['category_id' => $category->id]),
    		'only' => true
    	];
    	$this->template->data('groupMenus', $groupMenus);

    	$this->template->paint();

    }


    /* Debug cart */
    public function cart($method = '')
    {
        $methods = [
            'total',
            'total_items',
            'destroy'
        ];

        echo '<p><a href="' . module_url('cart') . '">View cart</a></p>';
        foreach ($methods as $m) {
            echo '<p><a href="' . module_url('cart/' . $m) . '">' . $m . '</a></p>';
        }

        echo '<br />';

        if ($method === '') {


            $this->load->model('Discounts');
            $res = $this->Discounts->apply('combo4');
            var_dump($res);

            $this->load->view('slices/cart/dump');
        } else {
            if (in_array($method, $methods)) {
                var_dump($this->cart->{$method}());
            } else {
                redirect(module_url('cart'));
            }
        }
    }


    public function checkout()
    {
        $slices = [
            'home/slider',
            'home/checkout',
            'home/product',
            'home/feedback'
        ];
        $this->template->slice($slices);

        $this->template->title('Trang chủ');        

        $this->template->paint();
    }


    public function user()
    {
		$slices = [
			'home/slider'
		];
		$this->template->slice($slices);

		$this->template->title('Profile');

		$this->template->paint();
    }
}
