<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Upload extends MX_Controller {

	public function index()
	{
        $this->load->library('Fileupload');
        $this->fileupload->handler();
	}
}
