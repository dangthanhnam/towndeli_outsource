<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Sms extends MX_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	function load()
	{
		$lines = file('import/SMS-05-08-09-10.csv');
		$this->db->truncate('sms');
		var_dump(count($lines));

		$dates = [
			1 => '2016-08-05',
			2 => '2016-08-08',
			3 => '2016-08-09',
			4 => '2016-08-10'
		];

		foreach ($lines as $idx => $line) {
			list($phone, $code) = explode(',', $line);
			$phone = trim($phone);
			$code = trim($code);
			$num = str_pad($idx+1, 3, 0, STR_PAD_LEFT);

			$content = 'TOWNDELI tang ban code BIG33-' . $num . ' GIAM NGAY 33% cho tat ca Order tu 3 Ly 22oz tro len. Khong gioi han so lan order den het ngay 13/08. Order ngay (08)73073777!';

			$data = [
				'phone' => $phone,
				'content' => $content,
				'schedule' => $dates[$code] . ' 13:30:00'
			];
			var_dump($data);

			$this->db->insert('sms', $data);
		}
	}


	function balance()
	{
		$this->load->model('Messages');
		$balance = $this->Messages->getBalance();
		echo 'Balance: ' . number_format($balance) . ' VND (~' . round($balance/300, 0, PHP_ROUND_HALF_DOWN) . ' SMS)';
	}
}
