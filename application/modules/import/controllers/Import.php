<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Import extends MX_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	function category()
	{
		$table = 'categories';
		$this->db->truncate($table);
		$lines = file('import/category.txt');

		foreach ($lines as $key => $value) {
			$cols = $this->parseData($value);
			$data = [
				'name' => $cols[1],
				'slug' => slug($cols[1])
			];
			$this->db->insert($table, $data);
		}
	}


	function menu()
	{
		$table = 'menus';
		$this->db->truncate($table);
		$lines = file('import/menu.txt');

		foreach ($lines as $key => $value) {
			$cols = $cols = $this->parseData($value);
			$data = [
				'category_id' => (int) $cols[1],
				'name' => mb_convert_case($cols[2], MB_CASE_TITLE, "UTF-8"),
				'short_description' => trim($cols[3], '"'),
				'price' => (int) $cols[4],
				'price_m' => (int) $cols[5],
				'price_l' => (int) $cols[6],
				'toppings' => $cols[7],
				'time_created' => date('Y-m-d H:i:s'),
				'status' => 1
			];
			var_dump($data);
			$this->db->insert($table, $data);
		}
	}


	function topping()
	{
		$table = 'toppings';
		$this->db->truncate($table);
		$lines = file('import/topping.txt');

		foreach ($lines as $key => $value) {
			$cols = $cols = $this->parseData($value);
			$data = [
				'name' => ucwords($cols[1]),
				'price' => (int) $cols[2],
				'time_created' => date('Y-m-d H:i:s'),
				'status' => 1
			];
			var_dump($data);
			$this->db->insert('toppings', $data);
		}
	}



	function order()
	{
		$this->db->truncate('users');
		$this->db->truncate('orders');


		// Admin users
		$salt = randomString(6);
		$admin = [
			'salt' => $salt,
			'password' => sha1('123towndeli' . $salt),
			'phone' => '0918609063',
			'role' => 1,
			'firstname' => 'Hưng',
			'gender' => 1,
			'time_created' => '2015-01-01 00:00:00'
		];
		$this->db->insert('users', $admin);
		$salt = randomString(6);
		$admin['password'] = sha1('123towndeli' . $salt);
		$admin['phone'] = '0974658440';
		$admin['firstname'] = 'Tân';
		$this->db->insert('users', $admin);



		$rows = file('import/lichsudonhang.txt');
		$orderNo = [];
		foreach ($rows as $key => $row) {
			$cols = preg_split('/[\t]/', $row);
			foreach ($cols as $key => $col) {
				$cols[$key] = preg_replace('~[\r\n]+~', '', trim($col, ' "\r\n'));
			}

			if (empty($orderNo[$cols[0]])) {
				$orderNo[$cols[0]] = 0;
			}


			/* USER */
			$phone = $cols[4];
			$name = mb_convert_case(mb_strtolower(trim($cols[3])), MB_CASE_TITLE);
			$gender = $cols[2] == 'NAM' ? 1 : 2;
			$chkUser = $this->db->where('phone', $phone)->get('users')->row();
			
			if (empty($chkUser)) {
				$salt = randomString(6);
				$userData = [
					'salt' => $salt,
					'password' => sha1($salt),
					'phone' => $phone,
					'role' => 9,
					'firstname' => $name,
					'gender' => $gender,
					'time_created' => '2015-01-01 00:00:00'
				];
				$this->db->insert('users', $userData);
				$userId = $this->db->insert_id();
			} else {
				$userId = (int) $chkUser->id;
				$this->db->where('id', $userId)->update('users', [
						'firstname' => $name,
						'gender' => $gender
					]);
			}

	        $idv = '';
	        while ($idv === '') {
	            $randomString = strtolower(randomString(6));
	            $chk = $this->db->where('idv', $randomString)->count_all_results('orders');
	            if ($chk === 0) {
	                $idv = $randomString;
	            }
	        }

			$data = [
				'idv' => $idv,
				'branch_id' => 1,
				'customer_id' => $userId,
				'order_number' => ++$orderNo[$cols[0]],
				'discount_code' => strtolower($cols[8]),
				'cart_subtotal' => intval(str_replace(',', '', $cols[6])),
				'cart_total' => intval(str_replace(',', '', $cols[9])),
				'cart_total_items' => 0,
				'customer_name' => $name,
				'customer_phone' => $phone,
				'points' => (isset($cols[10]) ? intval($cols[10]) : 0),
				'print_count' => 1,
				'time_created' => date('Y-m-d H:i:s', strtotime($cols[0] . $cols[1])),
				'status' => 4,
				'address ' => $cols[5]
			];
			$this->db->insert('orders', $data);

			// var_dump($data);
		}

		// $this->order_number();
	}


	function order_number()
	{
		$orders = $this->db->get('orders')->result();
		foreach ($orders as $row) {
			$no = $this->db
						->where('DATE(`time_created`)', date('Y-m-d', strtotime($row->time_created)))
						->where('id<=', $row->id)
						->count_all_results('orders');
			
			$this->db->where('id', $row->id)->update('orders', ['order_number' => $no]);
		}
	}


	function level_point()
	{
		$this->load->model('Users');
		$this->load->model('Orders');

		$this->db->set('points', 0)->update('orders');
		$this->db->set('level', 1)->update('users');

		$orderPoint = [];
		$userLevel = [];
		$userpoint = [];

		$users = $this->db->get('users')->result();
		foreach ($users as $key => $value) {
			$userLevel[$value->id] = 1;
			$userpoint[$value->id] = 0;
		}
		$orders = $this->db->get('orders')->result();

		foreach ($orders as $row) {
			$userId = $row->customer_id;
	        if (!isset($userLevel[$userId])) {
	            return 0;
	        }

	        $level = (int) $userLevel[$userId];
	        $point = 0;
	        $amount = (int) $row->cart_total;

	        if ($level <= 2) {
	            $point = floor($amount/30000);
	        } elseif ($level === 3) {
	            $point = floor($amount/25000);
	        } else {
	            $point = floor($amount/20000);
	        }
	        $orderPoint[$row->id] = (int) $point;
	        $userpoint[$userId] += intval($point);

	        $userTotalPoint = $userpoint[$userId];

	        if ($userTotalPoint <= 50) {
	            $level = 1;
	        } elseif ($userTotalPoint <= 700) {
	            $level = 2;
	        } elseif ($userTotalPoint <= 2000) {
	            $level = 3;
	        } else {
	            $level = 4;
	        }
	        $userLevel[$userId] = $level;
		}
		// echo '<pre>';
		// print_r($userLevel);
		// print_r($userpoint);
		// echo '</pre>';

		foreach ($orderPoint as $key => $value) {
			$this->db->where('id', $key)->set('points', $value)->update('orders');
		}

		foreach ($userLevel as $key => $value) {
			$this->db->where('id', $key)->set('level', $value)->update('users');
		}
	}


	function parseData($row)
	{
		$cols = preg_split('/[\t]/', $row);
		foreach ($cols as $key => $col) {
			$cols[$key] = preg_replace('~[\r\n]+~', '', trim($col, ' "\r\n'));
		}

		return $cols;
	}


}
