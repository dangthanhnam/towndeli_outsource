<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Ajax extends MX_Controller {

    private $posts;


	function __construct()
	{
		parent::__construct();

        $this->posts = $this->input->post();
	}


	public function index()
	{
        $posts = $this->input->post();
        foreach (['useModel', 'useAction'] as $item) {
            ${$item} = isset($posts[$item]) ? $posts[$item] : '';
            unset($posts[$item]);
        }

        $model = ucwords($useModel);
        $method = $useAction;
        $this->load->model($model);
        $result = $this->$model->$method();

        echo json_encode([
                'result' => !empty($result),
                'data' => $result
            ]);
	}
}
