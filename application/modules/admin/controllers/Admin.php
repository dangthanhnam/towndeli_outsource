<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Admin extends MX_Controller {


	private $segments = [];


	function __construct()
	{
		parent::__construct();

		$this->load->database();

		$this->load->library('Template');
		$this->load->library('Session');

		$this->segments = array_slice($this->uri->rsegments, 2);

        $this->load->library('Auth');
	}


	public function index()
	{
        if (!$this->auth->userId()) {
            redirect('admin/login');
        }

		$this->loadSlices();

		$title = 'Trang thống kê';
		$this->template->title($title);

		$breadcrumbs = [
			'<a href="' . module_url() . '">' . $title . '</a>'
		];
		$this->template->data('breadcrumbs', $breadcrumbs);

        $this->load->model('Report');

        $range = isset($_POST['range']) ? $_POST['range'] : '';
        if (!preg_match('#^(\d{4})-(\d{1,2})-(\d{1,2})$#', $range)) {
            $range = null;
        }
        $this->template->data('total', $this->Report->total());
        $this->template->data('report', $this->Report->totalByDate($range));

		$this->template->paint();
	}


    public function _remap($method)
    {
        if (method_exists($this, $method))
        {
            call_user_func_array(array($this, $method), $this->segments);
        }
        else
        {
        	$this->page($method);
        }
    }


    public function orders_menu()
    {
        $this->load->library('Cart');
        $this->cart->product_name_rules .= "\"\'\/\&";
        $posts = $this->input->post(null, true);

        if (isset($posts['rowid'])) {
            $cartContents = $this->cart->contents();
            $item = $cartContents[$posts['rowid']];
            $id = $item['id'];
            $size = $item['options']['size'];
            
            $posts['id'] = $id;
            $posts['size'] = $size;
        } else {
            $id = empty($posts['id']) ? 0 : intval($posts['id']);
            $size = empty($posts['size']) ? 0 : $posts['size'];
        }

        $this->load->model('Menus');
        $this->load->model('Toppings');

        $menu = $this->Menus->getDetail($id);

        if (is_null($menu)) {
            show_error('Menu is not found', 403);
        }

        $toppings = $this->Toppings->getListByIDs($menu->toppings);

        $data = [
            'row' => $menu,
            'toppings' => $toppings,
            'posts' => $posts
        ];

        $this->load->view('pages/orders_menu', $data);
    }


    public function orders_cart()
    {
        $this->load->view('pages/orders_cart');
    }


    public function orders_print()
    {
        $this->load->view('pages/orders_print');
    }


    private function page($method)
    {
        if (!$this->auth->userId()) {
            redirect('admin/login');
        }
        
    	// Get action (method)
    	$action = isset($this->segments[0]) ? $this->segments[0] : '';
    	// Get page info
    	$pageInfo = $this->getPageInfo($method);
    	if (!empty($pageInfo['slices'])) {
    		$this->template->slice($pageInfo['slices']);
    	}

    	// Set title
    	$title = isset($pageInfo['title']) ? $pageInfo['title'] : $method;
		$this->template->title($title);


		// Set breadcrumbs
		$breadcrumbs = [
			'<a href="' . module_url($method) . '">' . $title . '</a>'
		];
		if ($action !== '') {
			$title2_text = [
				'add' => 'Thêm',
				'edit' => 'Sửa'
			];
			$breadcrumbs[] = '<a href="' . module_url($method) . '">' . $title2_text[$action] . ' ' . $title . '</a>';
		}
		$this->template->data('breadcrumbs', $breadcrumbs);
		$this->template->data('action', $action);

		// Load model
        $model = ucfirst($method);
		$this->load->model($model);

        $options = [];
        if (!empty($_GET['page']) && $_GET['page'] > 1) {
            $options['page'] = (int) $_GET['page'];
        }
        $wheres = $this->dataFilters();
        $result = $this->$model->getList($wheres, $options);
        $this->template->data('data', $result['records']);
        $this->template->data('pagination', $this->load->view('slices/pagination', ['paging' => $result['paging']], true));

        $this->loadSlices();

        if ($action === 'edit') {

        	$action = 'add';
        	if (!isset($this->segments[1])) {
        		redirect(module_url($method));
        	}
        	$id = $this->segments[1];
        	$row = $this->$model->getDetail($id);
            if (is_null($row)) {
                redirect(module_url($method));
            }
        	$this->template->data('row', $row);

        }

		$page = $method . ($action !== '' ? '_' . $action : '');
		$this->template->paint($page);
    }


    private function dataFilters($filters = null)
    {
        if (empty($filters)) {
            $filters = $this->input->post_stream('filters');
        }
        $wheres = [];

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                $key = str_replace('__', '.', $key);
                $wheres[$key] = $value;
            }
        }

        return $wheres;
    }


    private function loadSlices()
    {
		$slices = [
			'head',
			'header',
			'footer',
			'page_sidebar'
		];
		$this->template->slice($slices);

		$this->template->data('sidebar_menu', $this->getPageInfo());
    }


    private function getPageInfo($page = NULL)
    {
    	$data = [
    		'branches' => [
				'title' => 'Chi nhánh',
				'icon' => 'icon-basket'
    		],
    		'categories' => [
    			'title' => 'Danh mục',
    			'icon' => 'icon-rocket'
    		],
    		'menus' => [
    			'title' => 'Thức uống',
    			'icon' => 'icon-diamond'
    		],
    		'toppings' => [
    			'title' => 'Món thêm',
    			'icon' => 'icon-diamond'
    		],
    		'materials' => [
    			'title' => 'Nguyên liệu',
    			'icon' => 'icon-diamond'
    		],
    		'orders' => [
    			'title' => 'Đơn hàng',
    			'icon' => 'icon-puzzle'
    		],
    		'users' => [
    			'title' => 'Thành viên',
    			'icon' => 'icon-user'
    		],
    		'sms' => [
    			'title' => 'Tin SMS',
    			'icon' => 'icon-user'
    		],
            'feedbacks' => [
                'title' => 'Phản hồi',
                'icon' => 'icon-puzzle'
            ],
    		'bugs' => [
    			'title' => 'Báo lỗi',
    			'icon' => 'icon-puzzle'
    		]
    	];


    	if (is_null($page)) {
    		return $data;
    	} else {
    		return isset($data[$page]) ? $data[$page] : [];
    	}
    }



    public function login()
    {
		$this->template->title('Đăng nhập');
		$this->template->layout('login_layout');

        $errorMessage = '';
        if (!empty($_POST)) {
            $this->load->model('Users');
            $res = $this->Users->login();
            if ($res) {
                redirect('admin');
            } else {
                $errorMessage = 'Email/Phone hoặc mật khẩu không đúng!';
            }
        }
        $this->template->data('errorMessage', $errorMessage);

        $this->template->paint();
    }


    public function logout()
    {
        $this->load->model('Users');
        $this->Users->logout();
        redirect('admin/login');
    }
}
