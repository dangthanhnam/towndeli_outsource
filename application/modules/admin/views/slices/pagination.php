<?php

$params = $_GET;

$current = $paging['page'];
$total = $paging['pages'];

$start = $current - 10;
$end = $current + 10;
if ($start < 1) {
	$start = 1;
}
if ($end > $total) {
	$end = $total;
}

?>

<ul class="pagination">
	<li>
		<a aria-label="Previous" href="?<?php $params['page'] = 1; echo http_build_query($params); ?>">
			<span aria-hidden="true">&laquo;</span>
		</a>
	</li>
<?php for ($i = $start; $i <= $end; $i++): $params['page'] = $i; ?>
	<li <?php if ($i === $current) echo 'class="active"' ?>><a href="?<?php echo http_build_query($params); ?>"><?php echo $i ?></a></li>
<?php endfor; ?>
	<li>
		<a aria-label="Next" href="?<?php $params['page'] = $total; echo http_build_query($params); ?>">
			<span aria-hidden="true">&raquo;</span>
		</a>
	</li>
</ul>