<title><?php echo $title ?> | TownDeli.VN</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content=""/>
	<meta name="author" content="Quốc Hưng Nguyễn"/>

	<?php echo chrome_frame(); ?>
	<?php echo view_port(); ?>
	<?php echo apple_mobile('black-translucent'); ?>
	<?php echo $meta; ?>

	<!-- icons and icons and icons and icons and icons and a tile -->
	<?php echo windows_tile(array('name' => 'Stencil', 'image' => base_url().'favicon.ico', 'color' => '#4eb4e5')); ?>
	<?php echo favicons(); ?>

	<?php echo shiv(); ?>

	<link href="<?php echo asset_url('global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?php echo asset_url('global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" />
	<link href="<?php echo asset_url('global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?php echo asset_url('global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" />
	<link href="<?php echo asset_url('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link href="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" />
	<link href="<?php echo asset_url('global/plugins/fullcalendar/fullcalendar.min.css') ?>" rel="stylesheet" />
	<link href="<?php echo asset_url('global/plugins/jqvmap/jqvmap/jqvmap.css') ?>" rel="stylesheet" />
	<link href="<?php echo asset_url('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') ?>" rel="stylesheet" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE STYLES -->
	<link href="<?php echo asset_url('admin/pages/css/tasks.css') ?>" rel="stylesheet" />
	<!-- END PAGE STYLES -->
	<!-- BEGIN THEME STYLES -->
	<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css') ?>' stylesheet instead of 'components.css' in the below style tag -->
	<link href="<?php echo asset_url('global/css/components.css') ?>" id="style_components" rel="stylesheet" />
	<!-- <link href="<?php echo asset_url('global/css/plugins.css') ?>" rel="stylesheet" /> -->
	<link href="<?php echo asset_url('admin/layout/css/layout.css') ?>" rel="stylesheet" />
	<link href="<?php echo asset_url('admin/layout/css/themes/darkblue.css') ?>" rel="stylesheet" id="style_color"/>
	<link href="<?php echo asset_url('admin/layout/css/custom.css') ?>" rel="stylesheet" />

	<link href="<?php echo asset_url('global/plugins/select2/css/select2.min.css') ?>" rel="stylesheet" />

	<!-- <link href="<?php echo asset_url('global/plugins/datepicker/css/datepicker.css') ?>" rel="stylesheet" /> -->

	<link href="<?php echo asset_url('global/css/common.css') ?>" rel="stylesheet" />
	<!-- END THEME STYLES -->