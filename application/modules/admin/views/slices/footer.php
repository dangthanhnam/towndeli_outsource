<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo (date('Y') > 2016 ? '2016 - ' : '') . date('Y') ?> &copy; Admin Cpanel
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>

<?php //View::partial('lock_screen') ?>

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo asset_url('global/plugins/respond.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/excanvas.min.js') ?>"></script> 
<![endif]-->
<script src="<?php echo asset_url('global/plugins/jquery.min.js') ?>"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo asset_url('global/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.blockui.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.cokie.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/uniform/jquery.uniform.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- <script src="<?php echo asset_url('global/plugins/jqvmap/jqvmap/jquery.vmap.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') ?>"></script> -->
<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.resize.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.categories.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.pulsate.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/moment.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="<?php echo asset_url('global/plugins/fullcalendar/fullcalendar.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.sparkline.min.js') ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo asset_url('global/scripts/metronic.js') ?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/layout.js') ?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/quick-sidebar.js') ?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/demo.js') ?>"></script>
<script src="<?php echo asset_url('admin/pages/scripts/index.js') ?>"></script>
<script src="<?php echo asset_url('admin/pages/scripts/tasks.js') ?>"></script>

<script src="<?php echo asset_url('global/plugins/fileupload/jquery.ui.widget.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/fileupload/jquery.iframe-transport.js') ?>"></script>
<script src="<?php echo asset_url('global/plugins/fileupload/jquery.fileupload.js') ?>"></script>

<script src="<?php echo asset_url('global/plugins/select2/js/select2.full.min.js') ?>"></script>

<script src="<?php echo asset_url('global/plugins/datepicker/js/bootstrap-datepicker.js') ?>"></script>

<script src="<?php echo asset_url('js/jQuery.print.js') ?>"></script>

<script>
<?php
	$method2Action = array(
			'add' => 'create',
			'edit' => 'update',
		);
?>
	var PARAMS = {
		siteURL: "<?php echo site_url() ?>",
		ajaxURL: "<?php echo module_url('ajax') ?>",
		moduleURL: "<?php echo module_url() ?>",
		model: "<?php echo $params->method ?>",
		method: "",
		action: "<?php echo isset($action) ? $action : '' ?>",
		uploadURL: ""
	};

	var API = {
		cart: "<?php echo site_url('api/cart') ?>"
	};

	var URL = {
		site: "<?php echo site_url() ?>",
		ajax: "<?php echo module_url('ajax') ?>",
		upload: "<?php echo site_url('upload') ?>",
		module: "<?php echo module_url() ?>"
	};
</script>
<script src="<?php echo asset_url('global/scripts/common.js') ?>"></script>
<script src="<?php echo asset_url('global/scripts/functions.js') ?>"></script>

<!--<script src="<?php echo asset_url('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') ?>"></script>-->

<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {

	Metronic.init(); // init metronic core componets
	Layout.init(); // init layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features 
	Index.init();   
	Index.initDashboardDaterange();
	//Index.initJQVMAP(); // init index page's custom scripts
	Index.initCalendar(); // init index page's custom scripts
	Index.initCharts(); // init index page's custom scripts
	Index.initChat();
	Index.initMiniCharts();
	Tasks.initDashboardWidget();


	x = $('.page-sidebar-menu >li >a');


	$('.page-sidebar-menu >li >a').each(function(i,v){
		/*
	    regex = '/admin$/gi';
	    eval('var regex = /'+ $(this).attr('href').replace('//', '\/\/') + '/;');

	    if (regex.test(url)) {
	        $(this).parent().addClass('active');
	    }
	    */
	});

});
</script>
<!-- END JAVASCRIPTS -->