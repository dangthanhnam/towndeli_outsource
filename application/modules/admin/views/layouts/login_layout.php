<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"/>
<link href="<?php echo asset_url('global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet"/>
<link href="<?php echo asset_url('global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet"/>
<link href="<?php echo asset_url('global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo asset_url('admin/pages/css/login3.css') ?>" rel="stylesheet"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo asset_url('global/css/components.css') ?>" id="style_components" rel="stylesheet"/>
<link href="<?php echo asset_url('global/css/plugins.css') ?>" rel="stylesheet"/>
<link href="<?php echo asset_url('admin/layout/css/layout.css') ?>" rel="stylesheet"/>
<link href="<?php echo asset_url('admin/layout/css/custom.css') ?>" rel="stylesheet"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="<?php echo site_url('favicon.ico') ?>"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="">
	<img href="<?php echo asset_url('admin/layout/img/logo-big.png') ?>" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<?php echo $content ?>
	<!-- END LOGIN FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<!-- <div class="copyright">
	 2014 &copy; Metronic. Admin Dashboard Template.
</div> -->
<!-- END COPYRIGHT -->

<!-- END PAGE LEVEL SCRIPTS -->
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>