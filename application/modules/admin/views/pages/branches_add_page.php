<style>
.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}
</style>
<?php 
	$this->load->model('Cities');
	$this->load->model('Districts');
	$cities = $this->Cities->getListAll([]);
	$cityID = $this->Cities->getCityByDistrict(@$row->location_id);
	$districts = $this->Districts->getListAll(['city_id' => $cityID]);
?>
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Thông tin chi tiết
			<a class="btn red btn-xs" href="<?php echo module_url($params->method . '/add') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
		</div>
		<div class="tools">
			<a class="reload" title="Làm mới dữ liệu"></a>
			<a class="remove" title="Xóa"></a>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<div class="form-horizontal Form_<?php echo $action ?>">
			<div class="form-body row">

				<div class="col-lg-6">
					<div class="form-group forEdit">
						<label class="col-md-3 control-label">ID</label>
						<div class="col-md-9">
							<span class="form-control-static idx"><?php echo @$row->id; ?></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Tên chi nhánh</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="name" value="<?php echo @$row->name ?>" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Khu vực</label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-lg-6 col-md-6">
									<select name="city" class="form-control locationCity" data-for="location_id">
										<option value="0">-- Chọn tỉnh/thành --</option>
										<?php foreach ($cities as $city): ?>
										<option value="<?php echo $city->id ?>" <?php if ($cityID == $city->id) echo 'selected' ?>><?php echo $city->name ?></option>
										<?php endforeach ?>
									</select>
								</div>

								<div class="col-lg-6 col-md-6">
									<select id="location_id" name="location_id" class="form-control">
										<option value="0">-- Chọn quận/huyện</option>
										<?php foreach ($districts as $district): ?>
										<option value="<?php echo $district->id ?>" <?php if (@$row->location_id == $district->id) echo 'selected' ?>><?php echo $district->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Địa chỉ</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="address" value="<?php echo @$row->address ?>" title="Hãy tìm vị trí bằng công cụ google map" disabled>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Định vị</label>
						<div class="col-md-9">
							<div class="input-group">
								<div class="input-group-addon" style="width: 80px">Vĩ độ</div>
								<input type="text" class="form-control" name="geo_latitude" value="<?php echo @$row->geo_latitude ?>" disabled>
							</div>
							<div class="input-group">
								<div class="input-group-addon" style="width: 80px">Kinh độ</div>
								<input type="text" class="form-control" name="geo_longitude" value="<?php echo @$row->geo_longitude ?>" disabled>
							</div>
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-3 control-label">Khu vực hỗ trợ</label>
						<div class="col-md-9">
							<select name="districts" class="form-control select2 tags" multiple="multiple">
								<option value="1">fsf</option>
								<option value="2">fsf</option>
								<option value="3">fsf</option>
							</select>
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-3 control-label">Trạng thái</label>
						<div class="col-md-9">
							<div class="md-checkbox has-success">
								<input type="checkbox" id="status" name="status" class="md-check"<?php if (@$row->status == 1 OR $action === 'add') echo ' checked' ?>>
								<label for="status"><span></span><span class="check"></span><span class="box"></span></label>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-6">
					<input id="pac-input" class="controls" type="text" placeholder="Tìm kiếm">
				    <div id="map" style="height: 300px"></div>
				    <script type="text/javascript">
						var positionDefault = {lat: 10.7989522, lng: 106.6848037};
						<?php
						if (@$row->geo_latitude != '' && @$row->geo_longitude != '') {
							echo 'positionDefault = {lat: ' . @$row->geo_latitude . ', lng: ' . @$row->geo_longitude . '};';
						}
						?>

						function initAutocomplete() {
							return;
							var map = new google.maps.Map(document.getElementById('map'), {
								center: positionDefault,
								zoom: 15,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							});

							var marker = new google.maps.Marker({
								position: positionDefault,
								map: map,
								draggable: true,
								title: 'TownDeli'
							});

							// Create the search box and link it to the UI element.
							var input = document.getElementById('pac-input');
							var searchBox = new google.maps.places.SearchBox(input);
							map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

							// Bias the SearchBox results towards current map's viewport.
							map.addListener('bounds_changed', function() {
								searchBox.setBounds(map.getBounds());
							});

							var markers = [];
							// Listen for the event fired when the user selects a prediction and retrieve
							// more details for that place.
							searchBox.addListener('places_changed', function() {
								var places = searchBox.getPlaces();

								if (places.length == 0) {
									return;
								}

								// Clear out the old markers.
								markers.forEach(function(marker) {
									marker.setMap(null);
								});
								markers = [];

								// For each place, get the icon, name and location.
								var bounds = new google.maps.LatLngBounds();
								places.forEach(function(place) {
									var icon = {
										//url: place.icon,
										size: new google.maps.Size(71, 71),
										origin: new google.maps.Point(0, 0),
										anchor: new google.maps.Point(17, 34),
										scaledSize: new google.maps.Size(25, 25)
									};

									// Create a marker for each place.
									markers.push(new google.maps.Marker({
										map: map,
										icon: icon,
										title: place.name,
										position: place.geometry.location,
										draggable: true
									}));

									if (place.geometry.viewport) {
										// Only geocodes have viewport.
										bounds.union(place.geometry.viewport);
									} else {
										bounds.extend(place.geometry.location);
									}
								});
								map.fitBounds(bounds);

								$('#address').val(input.value).data('origin', true);
								$('#geo_latitude').val(bounds.O.O).data('origin', true);
								$('#geo_longitude').val(bounds.j.j).data('origin', true);
							});


							google.maps.event.addListener(marker, 'drag', function(e) {
								$('#geo_latitude').val(e.latLng.lat()).data('origin', true);
								$('#geo_longitude').val(e.latLng.lng()).data('origin', true);
							});

							google.maps.event.addListener(marker, 'dragend', function(e) {
								$('#geo_latitude').val(e.latLng.lat()).data('origin', true);
								$('#geo_longitude').val(e.latLng.lng()).data('origin', true);
							});

						}

				    </script>
				    <script async defer src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCwjwM7AMRp_-D7kZe4M7fF7s0Sl76SUTQ&callback=initAutocomplete"></script>
				</div>

			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button class="btn btn-circle blue btnUpdate">Cập nhật</button>
						<button class="btn btn-circle default btnCancel">Hủy</button>
					</div>
				</div>
			</div>

		</div>
		<!-- END FORM-->
	</div>
</div>