<?php
	$this->load->model('Orders');
	$this->load->model('Toppings');
	$this->load->model('Users');
	$this->load->model('Branches');

	$branches = $this->Branches->getList()['records'];
	$statuses = $this->Orders->getStatuses();
	$employees = $this->Users->getEmployees();

	$menuSizes = [
		'price' => 12,
		'price_m' => 16,
		'price_l' => 22
	];
	
?>

<div class="portlet box green" data-model="<?php echo $params->method ?>">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cogs"></i><?php echo $title ?> 
			<a class="btn red btn-xs" href="<?php echo module_url($params->method . '/add') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
		</div>
		<div class="tools hidden">
			<a href="#portlet-config" data-toggle="modal" class="config" title="Thiết đặt"></a>
			<a class="reload" title="Làm mới dữ liệu"></a>
		</div>
	</div>

	<div class="row dataListSearch">
		<div class="col-lg-2 col-md-2">
			<select name="branch_id" id="" class="form-control" style="margin: 2%;width:96%">
				<option value="">Tất cả đơn hàng</option>
				<?php foreach ($branches as $row): ?>
				<option value="<?php echo $row->id ?>">Xem đơn hàng của chi nhánh <?php echo $row->name ?></option>
				<?php endforeach; ?>
			</select>
		</div>

		<div class="col-lg-2 col-md-2">
			<select name="orders__status" id="" class="form-control" style="margin: 2%;width:96%">
				<option value="">Tất cả trạng thái</option>
				<?php foreach ($statuses as $Key => $Value): ?>
				<option value="<?php echo $Key ?>"><?php echo $Value ?></option>
				<?php endforeach; ?>
			</select>
		</div>

		<div class="col-lg-4 col-md-4 DataListBtn">
			<a class="btn btn-md btn-success btnDataListSubmit">
				<span class="glyphicon glyphicon-search"></span> Lọc
			</a>
			<a class="btn btn-md btn-warning btnDataListReset">
				<span class="glyphicon glyphicon-refresh"></span> Reset
			</a>
		</div>
	</div>

	<div class="portlet-body flip-scroll" style="display: block;">
		<table class="table table-bordered table-striped table-condensed table-hover dataList">

			<?php echo $pagination ?>

			<thead class="flip-content">
				<tr>
					<th style="max-width: 25px"><input type="checkbox" class="selectAllRows"></th>
					<th style="max-width:10%">ID</th>
					<th style="max-width:18%">Khách hàng</th>
					<th style="max-width:25%">Đơn hàng</th>
					<th style="max-width:12%">Ghi chú giao hàng</th>
					<th style="max-width:12%">Trạng thái</th>
					<th style="max-width:12%">NV giao hàng</th>
					<th style="max-width:10%">Thao tác</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $row):
				$url = module_url($params->method . '/edit/' . $row->id);
				$cart = json_decode($row->cart_data, true);
				if (is_null($cart)) {
					$cart = [];
				}
			?>
				<tr data-id="<?php echo $row->id ?>">
					<td>
						<input type="checkbox" class="selectRow">
					</td>

					<td>
						<?php if ($row->status == 0): ?>
							<a href="<?php echo $url ?>"><b><?php echo $row->idv; ?></b></a>
						<?php else: ?>
							<p><b><?php echo $row->idv; ?></b></p>
						<?php endif; ?>

						<p>Số hiệu: <b class="orderNo"><?php echo str_pad($row->order_number, 3, '0', STR_PAD_LEFT) ?></b></p>
						<p><i><?php echo date('d/m/Y H:i', strtotime($row->time_created)) ?></i></p>
					</td>

					<td>
						<p><?php echo $row->customer_name ?></p>
						<p>Số ĐT: <b><?php echo $row->customer_phone ?></b></p>

						<?php if ($row->receiver_name != ''): ?>
						<p>Tên: <b><?php echo $row->receiver_name ?></b></p>
						<p>Số ĐT: <b><?php echo $row->receiver_phone ?></b></p>
						<p>L.Nhắn: <b><?php echo $row->receiver_message ?></b></p>
						<?php endif; ?>
						<p><?php echo $row->shipping_fulladdress ?></p>
					</td>

					<td>
						<p>Số lượng: <b><?php echo number_format($row->cart_total_items) ?></b></p>
						<?php if ($row->discount_code != '') echo '<p>Mã G.Giá: <i>' . $row->discount_code . '</i></p>' ?>
						<p>Tổng tiền: <b><?php echo number_format($row->cart_total) ?></b></p>
						<p>Điểm: [<b>-<?php echo $row->point_used ?></b>] <b>+<?php echo $row->points ?></b></p>

						<span class="btnShowMore">Chi tiết >></span>
						<div>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>T.T Món</th>
										<th>SL</th>
										<th>T.Tiền</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($cart as $rowId => $values): ?>
									<?php 
										$toppingData = [];
										if (!empty($values['options']['toppings'])) {
											$optionTopping = $values['options']['toppings'];
											$toppings = $this->Toppings->getListByIDs(array_keys($optionTopping));
											foreach ($toppings as $topping) {
												$toppingData[] = $topping->name . ' (' . $optionTopping[$topping->id] . ')';
											}
										}
									 ?>
									<tr>
										<td>
											<b><?php echo $values['name'] . ' [' . $menuSizes[$values['options']['size']] . 'oz]' ?></b>
											<?php if (!empty($toppingData)) echo '<div>+ <small>' . implode(', ', $toppingData) . '</small></div>' ?>
											<?php if (!empty($values['options']['note'])) echo '<div><small><i style="color:red">** ' . $values['options']['note'] . '</i></small></div>' ?>
										</td>
										<td class="text-right"><?php echo $values['qty'] ?></td>
										<td class="text-right"><?php echo number_format($values['subtotal']) ?></td>
									</tr>
									<?php endforeach; ?>

									<tr>
										<td colspan="3">
											<?php if ($row->discount_code != ''): ?>
											<p>Mã giảm giá: <b><?php echo $row->discount_code ?></b></p>
											<?php endif ?>
											<p>Tạm tính: <b><?php echo number_format($row->cart_subtotal) ?> vnđ</b></p>
											<p>Tổng cộng: <b><?php echo number_format($row->cart_total) ?> vnđ</b></p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</td>

					<td>
						<?php if (!empty($row->note)): ?>
							<p>KH: <?php echo $row->note ?></p>
						<?php endif; ?>

						<?php if (!empty($row->note_admin)): ?>
							<p>Town: <?php echo $row->note_admin ?></p>
						<?php endif; ?>

						<p>Giao lúc: <?php echo $row->shipping_on != '' ? $row->shipping_on : date('H:i', strtotime($row->time_created)+35*60) ?></p>
					</td>

					<td>
						<select class="form-control changeByStep" name="status" <?php if ($row->status == $this->Orders->getLastStatus()) echo 'disabled' ?>>
							<?php foreach ($this->Orders->getStatuses() as $statusNo => $statusName): ?>
							<option value="<?php echo $statusNo ?>" <?php if ($row->status == $statusNo) echo 'selected' ?> <?php if ($statusNo < $row->status OR $statusNo >= $row->status + 2) echo 'disabled' ?>><?php echo $statusName ?></option>
							<?php endforeach; ?>
						</select>
					</td>

					<td>
						<select class="form-control" name="shipper_id">
							<option value="">-- Chọn NV giao --</option>
							<?php foreach ($employees as $user): ?>
							<option value="<?php echo $user->id ?>" <?php if (@$row->shipper_id == $user->id) echo 'selected' ?>><?php echo $user->firstname ?></option>
							<?php endforeach; ?>
						</select>
					</td>

					<td class="actions">
						<a class="btn btn-xs btn-success btnOrderPreview"><span class="glyphicon glyphicon-print"></span></a>
						<?php if ($row->status <= 2): ?>
						<a class="btn btn-xs btn-danger btnOrderCancel"><span class="glyphicon glyphicon-trash"></span></a>
						<?php endif ?>
						<?php if (empty($row->shipping_delay) && $row->status <= 2): ?>
						<a class="btn btn-xs btn-warning btnOrderDelay"><span class="glyphicon glyphicon-alert"></span></a>
						<?php endif ?>
						<p>In <b><?php echo $row->print_count ?></b> lần</p>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<?php echo $pagination ?>

	</div>
</div>