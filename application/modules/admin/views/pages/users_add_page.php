<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Thông tin chi tiết
			<a class="btn red btn-xs" href="<?php echo module_url($params->method . '/add') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
		</div>
		<div class="tools">
			<a class="reload" title="Làm mới dữ liệu"></a>
			<a class="remove" title="Xóa"></a>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<div class="form-horizontal Form_<?php echo $action ?>">
			<div class="form-body row">

				<!-- BLOCK LEFT -->
				<div class="col-lg-8">
					<div class="form-group forEdit">
						<label class="col-md-4 control-label">ID</label>
						<div class="col-md-8">
							<span class="form-control-static idx"><?php echo @$row->id; ?></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Họ tên</label>
						<div class="col-md-8">
							<div class="row">
								<div class="col-lg-6 col-md-6">
									<input type="text" class="form-control" name="firstname" value="<?php echo @$row->firstname ?>" placeholder="Tên gọi">
								</div>
								<div class="col-lg-6 col-md-6">
									<input type="text" class="form-control" name="lastname" value="<?php echo @$row->lastname ?>" placeholder="Họ và tên lót">
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Số ĐT</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="phone" value="<?php echo @$row->phone ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Username</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="username" value="<?php echo @$row->username ?>" <?php if (@$row->username !== '') echo 'disabled' ?>>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Email</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="email" class="form-control" name="email" value="<?php echo @$row->email ?>" <?php if (isset($row)) echo 'disabled' ?>>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Mật khẩu</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="password" class="form-control" name="password">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Loại tài khoản</label>
						<div class="col-md-8">
							<select name="role" class="form-control" required>
								<option value="0">-- Chọn --</option>
								<?php foreach ($this->Users->getRoles() as $key => $value): ?>
								<option value="<?php echo $key ?>" <?php if (@$row->role == $key) echo 'selected' ?>><?php echo $value ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="form-group hidden">
						<label class="col-md-4 control-label">Ngày sinh</label>
						<div class="col-md-8">
							<input type="text" class="form-control" value="<?php echo @$row->birthday ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Giới tính</label>
						<div class="col-md-8">
							<input type="hidden" id="gender" name="gender" value="<?php echo @$row->gender ?>" />
							<div class="md-radio-inline">
								<div class="md-radio" data-value="1">
									<input type="radio" id="gender1" name="gender" class="md-radiobtn" <?php if (@$row->gender == 1) echo 'checked' ?>>
									<label for="gender1">
									<span></span>
									<span class="check"></span>
									<span class="box"></span>
									Nam </label>
								</div>
								<div class="md-radio" data-value="2">
									<input type="radio" id="gender2" name="gender" class="md-radiobtn" <?php if (@$row->gender == 2) echo 'checked' ?>>
									<label for="gender2">
									<span></span>
									<span class="check"></span>
									<span class="box"></span>
									Nữ </label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Địa chỉ nhà</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="address_home" value="<?php echo @$row->address_home ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Địa chỉ cty</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="address_office" value="<?php echo @$row->address_office ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Facebook</label>
						<div class="col-md-8">
							<input type="url" class="form-control" name="page_facebook" value="<?php echo @$row->page_facebook ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Twitter</label>
						<div class="col-md-8">
							<input type="url" class="form-control" name="page_twitter" value="<?php echo @$row->page_twitter ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Trạng thái</label>
						<div class="col-md-8">
							<div class="md-checkbox has-success">
								<input type="checkbox" id="status" name="status" class="md-check"<?php if (@$row->status == 1 OR $action === 'add') echo ' checked' ?>>
								<label for="status"><span></span><span class="check"></span><span class="box"></span></label>
							</div>
						</div>
					</div>

				</div>
				<!-- END: BLOCK LEFT -->

				<div class="col-lg-4">
						<span class="btn btn-sm btn-success btnUpload">
						    <i class="glyphicon glyphicon-upload"></i>
						    <span>Tải lên</span>
						</span>
						<input class="fileupload" type="file" name="files[]" data-unique="<?php echo md5(time() . randomString(10)) ?>">
						<div class="progress">
							<div class="progress-bar"></div>
						</div>
						<a class="thumbnail previewUpload"><?php echo getCoverImage($params->method, @$row->id) ?></a>
				</div>
			</div>

			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button class="btn btn-circle blue btnUpdate">Cập nhật</button>
						<button class="btn btn-circle default btnCancel">Hủy</button>
					</div>
				</div>
			</div>

		</div>
		<!-- END FORM-->
	</div>
</div>