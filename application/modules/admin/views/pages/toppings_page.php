<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cogs"></i><?php echo $title ?> 
			<a class="btn red btn-xs" href="<?php echo module_url($params->method . '/add') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
		</div>
		<div class="tools">
			<a class="reload" title="Làm mới dữ liệu"></a>
		</div>
	</div>
	<div class="portlet-body flip-scroll" style="display: block;">
		<table class="table table-bordered table-striped table-condensed">

			<?php echo $pagination ?>

			<thead class="flip-content">
				<tr>
					<th style="width: 25px"><input type="checkbox" class="selectAllRows"></th>
					<th>ID</th>
					<th>Tên nguyên liệu</th>
					<th>Mô tả</th>
					<th>Trạng thái</th>
					<th>Thao tác</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $row):
				$url = module_url($params->method . '/edit/' . $row->id);
			?>
				<tr data-id="<?php echo $row->id ?>">
					<td><input type="checkbox" class="selectRow"></td>
					<td class="text-right"><?php echo $row->id ?></td>
					<td><a href="<?php echo $url ?>"><?php echo $row->name ?></a></td>
					<td><?php echo $row->description ?></td>
					<td>
						<div class="md-checkbox has-success">
							<input type="checkbox" id="cbox_<?php echo $row->id ?>" class="md-check"<?php echo $row->status == 1 ? ' checked' : '' ?>>
							<label for="cbox_<?php echo $row->id ?>"><span></span><span class="check"></span><span class="box"></span></label>
						</div>
					<td class="actions">
						 <button type="button" class="btn btn-xs red btnDelete"><span class="glyphicon glyphicon-trash"></span></button>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<?php echo $pagination ?>

	</div>
</div>