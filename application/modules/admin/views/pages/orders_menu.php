<?php

$sizeMenus = [
	'price' => 12,
	'price_m' => 16,
	'price_l' => 22
];

if (isset($posts['rowid'])) {
	$cartData = $this->cart->contents();
	$item = $cartData[$posts['rowid']];
	$itemtoppings = isset($item['options']['toppings']) ? $item['options']['toppings'] : [];
	$itemQty = $item['qty'];
} else {
	$itemtoppings = [];
	$itemQty = 1;
	$posts['rowid'] = '';
}


$sizes = [];
foreach ($sizeMenus as $fieldSize => $value) {
	if ($row->{$fieldSize} != 0) {
		$sizes[] = '<a class="btn btn-md btn-success' . ($fieldSize == $posts['size'] ? ' selected' : '') . '" data-field="' . $fieldSize . '">' . $value . 'oz</a>';
	}
}

?>
<div data-id="<?php echo $row->id ?>" data-rowid="<?php echo $posts['rowid'] ?>">
	<h3><?php echo $row->name ?></h3>
	<p class="ordersMenuSizes">Chọn size: <?php echo implode('', $sizes) ?></p>
	<div>
		<div class="input-group">
		  <span class="input-group-addon">Số lượng</span>
		  <input type="number" class="form-control" id="ordersMenuQuantity" value="<?php echo $itemQty ?>" min="1" style="width: 80px">
		</div>
	</div>
	<br />
	<div class="ordersMenuTopping">
		<?php foreach ($toppings as $topping): ?>
			<a class="btn btn-sm btn-default" data-id="<?php echo $topping->id ?>"><span class="badge"><?php echo isset($itemtoppings[$topping->id]) ? $itemtoppings[$topping->id] : 0 ?></span> <?php echo $topping->name ?> <span class="btn btn-xs btn-danger btnOrdersToppingDecrease"><i class="glyphicon glyphicon-minus" aria-hidden="true"></i></span></a>
		<?php endforeach; ?>
	</div>
	<div>
		<label for="ordersMenuNote"><b>Ghi chú</b></label>
		<textarea id="ordersMenuNote" class="form-control" cols="30" rows="3"></textarea>
	</div>
</div>