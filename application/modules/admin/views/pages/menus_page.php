<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cogs"></i><?php echo $title ?> 
			<a class="btn red btn-xs" href="<?php echo module_url($params->method . '/add') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
			<a class="btn btn-success btn-xs btnAddToppings"><i class="glyphicon glyphicon-plus"></i> Chọn topping</a>
		</div>
		<div class="tools">
			<a href="#portlet-config" data-toggle="modal" class="config" title="Thiết đặt"></a>
			<a class="reload" title="Làm mới dữ liệu"></a>
		</div>
	</div>
	<div class="portlet-body flip-scroll" style="display: block;">
		<table class="table table-bordered table-striped table-condensed">

			<?php echo $pagination ?>

			<thead class="flip-content">
				<tr>
					<th style="width: 25px"><input type="checkbox" class="selectAllRows"></th>
					<th>ID</th>
					<th>Tên thức uống</th>
					<th>Đơn giá</th>
					<th>Số lượng</th>
					<th>GIảm giá</th>
					<th>Lượt xem</th>
					<th>Toppings</th>
					<th>Trạng thái</th>
					<th>Thao tác</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $row):
				$url = module_url($params->method . '/edit/' . $row->id);
			?>
				<tr data-id="<?php echo $row->id ?>">
					<td><input type="checkbox" class="selectRow"></td>
					<td class="text-right"><?php echo $row->id ?></td>
					<td><a href="<?php echo $url ?>"><?php echo $row->name ?></a></td>
					<td>
						S: <b><?php echo number_format($row->price) ?></b>đ; 
						M: <b><?php echo number_format($row->price_m) ?></b>đ; 
						L: <b><?php echo number_format($row->price_l) ?></b>đ;
					</td>
					<td class="text-right"><?php echo number_format($row->quantity) ?></td>
					<td>&nbsp;</td>
					<td class="text-right"><?php echo number_format($row->views) ?></td>
					<td><?php echo $row->toppings != '' ? '<span class="glyphicon glyphicon-ok"></span>' : ''; ?></td>
					<td>
						<div class="md-checkbox has-success">
							<input type="checkbox" id="cbox_<?php echo $row->id ?>" class="md-check"<?php echo $row->status == 1 ? ' checked' : '' ?>>
							<label for="cbox_<?php echo $row->id ?>"><span></span><span class="check"></span><span class="box"></span></label>
						</div>
					<td class="actions">
						 <button type="button" class="btn btn-xs red btnDelete"><span class="glyphicon glyphicon-trash"></span></button>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<?php echo $pagination ?>

	</div>
</div>

<div id="modal_addToppings" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Thêm toppings</h4>
      </div>
      <div class="modal-body">
		<ul class="listTopping">
			<?php foreach ($rows as $item): ?>
			<li value="<?php echo $item->id ?>"><?php echo $item->name ?></li>
			<?php endforeach ?>
		</ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
        <button type="button" class="btn btn-primary btn_addToppings_save">Lưu</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->