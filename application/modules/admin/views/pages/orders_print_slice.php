<?php

$this->load->model('Orders');
$this->load->model('Toppings');
$this->load->model('Discounts');
$this->load->model('Districts');
$this->load->model('Users');

$row = $this->Orders->getDetail($id);
$cart = json_decode($row->cart_data, true);
if (empty($cart)) {
	$cart = [];
}

$user = $this->Users->getDetail($row->customer_id);

$area = $this->Districts->getDetail($row->shipping_district);
if ($area) {
	$areaText = $row->shipping_ward . ', ' . $area->name;
} else {
	$areaText = '';
}

$sizes = [
	'price' => 12,
	'price_m' => 16,
	'price_l' => 22
];

$i = 1;
$discountCode = empty($row->discount_code) ? '' : $row->discount_code;
if ($discountCode !== '') {
	$discount = $this->Discounts->getDetailByField('code', $discountCode);
}

?>

<table style="width: 100%">
	<tr>
		<td style="width: 10%">
			<a class="NoOrderPrint" style="color:#000;cursor:default;text-decoration:none">
			<?php echo str_pad($row->order_number, 3, '0', STR_PAD_LEFT) ?>
			</a>
		</td>
		<td style="width: 20%">
			<img src="<?php echo asset_url('frontend/images/logo-towndeli.png') ?>" alt="logo" />
		</td>
		<td class="orderPrintTitle" style="width: 70%">
			<h3>HÓA ĐƠN #<?php echo strtoupper($row->idv) ?></h3>
			<p><?php echo date('d/m/Y - H:i') ?></p>
			<p class="orderPrintContact"><b>http://towndeli.vn - (08) 73.073.777</b></p>
		</td>
	</tr>
</table>

<table class="table table-condensed">
	<thead>
		<tr>
			<th class="text-right" style="width: 5%">#</th>
			<th style="width: 45%">Thông tin món</th>
			<th class="text-right" style="width: 10%">SL</th>
			<th class="text-right" style="width: 20%">Tạm tính</th>
			<th style="width: 20%">Ghi chú</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($cart as $rowId => $item): ?>
		<tr>
			<td class="text-right"><?php echo $i++ ?></td>
			<td>
				<?php
					echo $item['name'] . ' <b>[' . $sizes[$item['options']['size']] . 'oz]</b>';
					$options = $item['options'];
					if (isset($options['toppings'])) {
						$this->load->model('Toppings');
						$optionTopping = $options['toppings'];
						$toppings = $this->Toppings->getListByIDs(array_keys($optionTopping));
						$toppingData = [];
						foreach ($toppings as $topping) {
							echo '<div><small>+ ' . $topping->name . ' (' . $optionTopping[$topping->id] . ')' . '</small></div>';
						}
					}
				?>
			</td>
			<td class="text-right"><?php echo number_format($item['qty']) ?></td>
			<td class="text-right"><?php echo number_format($item['subtotal']) ?> đ</td>
			<td><?php if (!empty($options['note'])) echo '<small><b><i>' . $options['note'] . '</i></b></small>' ?></td>
		</tr>
		<?php endforeach ?>

		<?php if ($discountCode !== ''): ?>
		<tr>
			<td></td>
			<td class="text-cenetr">
				<i>Tạm tính</i>
			</td>
			<td></td>
			<td class="text-right">
				<i><?php echo number_format($row->cart_subtotal) ?> đ</i>
			</td>
			<td></td>
		</tr>

		<tr>
			<td></td>
			<td colspan="2">
				<i>Mã KM <b><?php echo $discountCode ?></b></i></b>
			</td>
			<td class="text-right">
				<i><?php echo number_format($row->cart_total - $row->cart_subtotal) ?> đ</i>
			</td>
			<td>
				<i><?php echo $discount->description ?></i>
			</td>
		</tr>
		<?php endif ?>

		<tr style="font-size: 1.1em">
			<td></td>
			<td>
				<b>TỔNG CỘNG</b>
			</td>
			<td class="text-right">
				<b><?php echo number_format($row->cart_total_items) ?></b>
			</td>
			<td class="text-right">
				<b><?php echo number_format($row->cart_total) ?> đ</b>
			</td>
			<td></td>
		</tr>

	</tbody>
</table>

<table class="table table-condensed">
	<tr>
		<td valign="top" style="width:50%">
			<p><?php echo (@$user->gender == 1 ? 'Anh ' : 'Chị ') . @$user->firstname . ' (' . @$row->customer_phone . ')' ?></p>
			<p><?php echo @$row->shipping_address ?></p>
			<p><?php echo $areaText ?></p>
			<p><?php echo @$row->shipping_floor ?></p>
		</td>
		<td valign="top" style="width:50%">
			<p>Giao lúc: <b><?php echo empty($row->shipping_on) ? 'Sớm có thể': $row->shipping_on; ?></b></p>
			<p><?php echo !empty($row->note) ? $row->note : $row->note ?></p>
		</td>
	</tr>
</table>