<?php
	$id = $this->input->post('id');
	$slice = $this->load->view('pages/orders_print_slice', ['id' => $id], true);
?>

<div id="orderPrintBLock">
	<div class="no-print"><?php echo $slice ?></div>
	<div class="orderPrintDivider"></div>
	<table class="orderPrintContent">
		<tr>
			<td valign="top" style="width:46%;padding: 0 2%"><?php echo $slice ?></td>
			<td valign="top" style="width:46%;padding: 0 2%"><?php echo $slice ?></td>
		</tr>
	</table>
</div>

<div class="text-center">
	<a class="btn btn-md btn-success btnOrderPrint" accesskey="e" data-id="<?php echo $id ?>">
		<span class="glyphicon glyphicon-print"></span> In đơn hàng này
	</a>
</div>