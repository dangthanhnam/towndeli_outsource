<?php
	$this->load->library('Cart');
	$this->cart->product_name_rules .= "\"\'\/\&";
	$this->load->library('session');
	$this->load->model('Discounts');
	$discounts = $this->Discounts->getAvailable();

	$stt = 1;
	$discountInfo = '';

	if (!empty($row)) {
		$discountCode = strval($row->discount_code);
		$cart = json_decode($row->cart_data, true);
		if ($cart) {
			$this->cart->destroy();
			foreach ($cart as $rowId => $cartItem)
			{
				unset($cartItem['rowid'], $cartItem['subtotal']);
				$this->cart->insert($cartItem);
			}

			$this->session->set_userdata('discount_code', $row->discount_code);
		}
	} else {
		$discountCode = strval($this->session->userdata('discount_code'));
	}
	
	$discountResult = $this->Discounts->check($discountCode);

	if ($discountResult > 0) {
		$totalAfterDiscount = $discountResult;
		$discountRes = $this->Discounts->getDetailByField('code', $discountCode);
		$discountInfo = '<label style="color:red">' . $discountRes->name . '</label><p style="color:#3ea49d">' . $discountRes->description . '</p>';
	} else {
		$totalAfterDiscount = $this->cart->total();
	}

	$menuSizes = [
		'price' => 12,
		'price_m' => 16,
		'price_l' => 22
	];
?>
<table class="table" data-order-id="<?php echo @$row->id ?>">
	<thead>
		<tr>
			<th>#</th>
			<th>Thông tin món</th>
			<th>SL</th>
			<th>T.Tính</th>
			<th style="width:80px">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($this->cart->contents() as $rowID => $row):
			$itemOptions = $this->cart->product_options($rowID);
		?>
		<tr data-id="<?php echo $rowID ?>">
			<td>
				<?php echo $stt++ ?>
			</td>
			<td>
				<?php
				echo '<b>' . $row['name'] . ' (' . $menuSizes[$itemOptions['size']] .  'oz)' . '</b>';
				if (isset($itemOptions['toppings'])) {
					$this->load->model('Toppings');
					$optionTopping = $itemOptions['toppings'];
					$toppings = $this->Toppings->getListByIDs(array_keys($optionTopping));
					$toppingData = [];
					foreach ($toppings as $topping) {
						$toppingData[] = $topping->name . ' (' . $optionTopping[$topping->id] . ')';
					}
					echo '<div><small>+ ' . implode(', ', $toppingData) . '</small></div>';
				}

				if (!empty($row['options']['note'])) {
					echo '<div><small><i style="color:red">** ' . $row['options']['note'] . '</i></small></div>';
				}
				?>
			</td>
			<td class="text-right">
				<select name="orderItemQty">
					<?php for ($i = 1; $i <= 50; $i++): ?>
						<option value="<?php echo $i ?>" <?php if ($i == $row['qty']) echo 'selected' ?>><?php echo $i ?></option>
					<?php endfor; ?>
				</select>
			</td>
			<td class="text-right">
				<b><?php echo number_format($row['subtotal']/1000) ?>K</b>
			</td>
			<td>
				<a class="btn btn-xs btn-success btnOrdersCartEdit"><span class="glyphicon glyphicon-edit"></span></a>
				<a class="btn btn-xs btn-danger btnOrdersCartRemove"><span class="glyphicon glyphicon-remove"></span></a>
			</td>
		</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="2">Tạm tính</td>
			<td colspan="3"><strong><?php echo number_format($this->cart->total()); ?> đ</strong></td>
		</tr>
		<tr>
			<td colspan="5">
				<div class="orderCartDiscounts">
				<?php foreach ($discounts as $row): ?>
					<a class="btn btn-xs btn-warning" title="<?php echo $row->name . ' (' . $row->description . ')' ?>"><?php echo $row->code ?></a>
				<?php endforeach; ?>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				Mã khuyến mãi <input type="text" class="form-control orderCartDiscountCode" value="<?php echo $discountCode ?>" placeholder="Nhập mã rồi Enter">
				<div style="font-weight:700;font-size:0.95em">
					<?php echo $discountInfo; ?>
				</div>
			</td>
		</tr>
		<tr class="orderUsePoint">
			<td colspan="5">Dùng điểm thưởng <input type="text" class="form-control"></td>
		</tr>
		<tr>
			<td colspan="2">Tổng cộng</td>
			<td><b><?php echo $this->cart->total_items() ?></b></td>
			<td colspan="2"><strong><?php echo number_format($totalAfterDiscount); ?> đ</strong></td>
		</tr>
	</tbody>
</table>

<div class="text-center">
	<a class="btn btn-md btn-primary btnCustomerInfoToggle"><span class="glyphicon glyphicon-user"></span> T.T K.Hàng</a>
	<a class="btn btn-md btn-success btnOrderCreate <?php if ($this->cart->total_items() === 0) echo 'disabled' ?>"><span class="glyphicon glyphicon-send"></span> Tạo/Sửa Đ.Hàng</a>

	<div style="margin-top: 20px">
		<a class="btn btn-md btn-danger btnOrderClean <?php if ($this->cart->total_items() === 0) echo 'disabled' ?>"><span class="glyphicon glyphicon-trash"></span> HỦY</a>
	</div>
</div>

