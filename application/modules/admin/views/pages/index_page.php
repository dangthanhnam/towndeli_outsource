<div class="row">
	<div class="col-lg-2">
		<table class="table">
			<?php foreach ($total as $key => $value): ?>
			<tr>
				<td style="width: 100px"><?php echo $key ?></td>
				<td style="text-align: right"><?php echo number_format($value) ?></td>
			</tr>
			<?php endforeach ?>
		</table>
	</div>

	<div class="col-lg-10">

		<div class="input-group" style="width: 400px">
			<input type="text" name="reportDate" class="form-control datePicker" placeholder="YYYY-MM-DD">
			<span class="input-group-btn">
				<a class="btn btn-success btnReport">Xem theo ngày</a>
				<a class="btn btn-danger btnReportAll">Xem tất cả</a>
			</span>
		</div>

		<div class="reportData">
			<table class="table">
				<?php foreach ($report['orders'] as $key => $value): ?>
				<tr>
					<td style="width: 100px"><?php echo $key ?></td>
					<td style="text-align: right"><?php echo number_format($value) ?></td>
				</tr>
				<?php endforeach ?>
			</table>

			<table class="table">
				<thead>
					<tr>
						<th>Tên</th>
						<th style="width: 120px">Số đơn hàng</th>
						<th style="width: 120px">Số món</th>
						<th style="width: 150px">Tổng tiền</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($report['shippers'] as $key => $row): ?>
					<tr>
						<td><?php echo $row->firstname . ' (' . $row->phone .')' ?></td>
						<td style="text-align: right"><?php echo number_format($row->order_count) ?></td>
						<td style="text-align: right"><?php echo number_format($row->total_item) ?></td>
						<td style="text-align: right"><?php echo number_format($row->order_total) ?></td>
					</tr>
					<?php endforeach ?>
					<tr>
						<td></td>
						<td style="text-align: right"><b><?php echo number_format($report['orderFinish']) ?></b></td>
						<td style="text-align: right"><b><?php echo number_format($report['orderItem']) ?></b></td>
						<td style="text-align: right"><b><?php echo number_format($report['revenue']) ?></b></td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
</div>