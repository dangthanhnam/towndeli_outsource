<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cogs"></i><?php echo $title ?> 
			<a class="btn red btn-xs" href="<?php echo module_url($params->method . '/add') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
		</div>
		<div class="tools">
			<a href="#portlet-config" data-toggle="modal" class="config" title="Thiết đặt"></a>
			<a class="reload" title="Làm mới dữ liệu"></a>
		</div>
	</div>
	<div class="portlet-body flip-scroll" style="display: block;">

		<?php echo $pagination ?>

		<table class="table table-bordered table-striped table-condensed">
			<thead class="flip-content">
				<tr>
					<th style="width: 25px"><input type="checkbox" class="selectAllRows"></th>
					<th>ID</th>
					<th>Họ tên</th>
					<th>Số điện thoại</th>
					<th>Email</th>
					<th>Loại T.Khoản</th>
					<th>Cấp độ</th>
					<th>Ngày sinh</th>
					<th>Trạng thái</th>
					<th>Thao tác</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $row):
				$url = module_url($params->method . '/edit/' . $row->id);
			?>
				<tr data-id="<?php echo $row->id ?>">
					<td><input type="checkbox" class="selectRow"></td>
					<td><?php echo $row->id ?></td>
					<td><a href="<?php echo $url ?>"><?php echo $row->lastname . ' ' . $row->firstname ?></a></td>
					<td><?php echo $row->phone ?></td>
					<td><?php echo $row->email ?></td>
					<td><?php echo $this->Users->getRoles(strval($row->role)) ?></td>
					<td><?php if ($row->role == 9) echo str_repeat('<span class="glyphicon glyphicon-star"></span>', $row->level) ?></td>
					<td><?php echo $row->birthday ?></td>
					<td>
						<div class="md-checkbox has-success">
							<input type="checkbox" id="checkbox_<?php echo $row->id ?>" class="md-check"<?php echo $row->status == 1 ? ' checked' : '' ?>>
							<label for="checkbox_<?php echo $row->id ?>"><span></span><span class="check"></span><span class="box"></span></label>
						</div>
					<td class="actions">
						 <button type="button" class="btn btn-xs red btnDelete"><span class="glyphicon glyphicon-trash"></span></button>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<?php echo $pagination ?>

	</div>
</div>