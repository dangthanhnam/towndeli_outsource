<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Thông tin chi tiết
			<a class="btn red btn-xs" href="<?php echo module_url($params->method . '/add') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
		</div>
		<div class="tools">
			<a class="reload" title="Làm mới dữ liệu"></a>
			<a class="remove" title="Xóa"></a>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<div class="form-horizontal Form_<?php echo $action ?>">
			<div class="form-body row">

				<div class="col-lg-8">
					<div class="form-group forEdit">
						<label class="col-md-4 control-label">ID</label>
						<div class="col-md-8">
							<span class="form-control-static idx"><?php echo @$row->id; ?></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Tên topping</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="name" value="<?php echo @$row->name ?>" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Mô tả</label>
						<div class="col-md-8">
							<textarea class="form-control" name="description" cols="30" rows="5"><?php echo @$row->description ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Đơn giá</label>
						<div class="col-md-8">

							<div class="input-group">
								<input type="number" class="form-control" name="price" value="<?php echo @$row->price ? @$row->price : 5000 ?>" required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Trạng thái</label>
						<div class="col-md-8">
							<div class="md-checkbox has-success">
								<input type="checkbox" id="status" name="status" class="md-check"<?php if (@$row->status == 1 OR $action === 'add') echo ' checked' ?>>
								<label for="status"><span></span><span class="check"></span><span class="box"></span></label>
							</div>
						</div>
					</div>

				</div>

				<div class="col-lg-4">
					<span class="btn btn-sm btn-success btnUpload">
					    <i class="glyphicon glyphicon-upload"></i>
					    <span>Tải lên</span>
					</span>
					<input class="fileupload" type="file" name="files[]" data-unique="<?php echo md5(time() . randomString(10)) ?>">
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a class="thumbnail previewUpload"><?php //echo getCoverImage(Router::$controller, $row->id) ?></a>
				</div>

			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button class="btn btn-circle blue btnUpdate">Cập nhật</button>
						<button class="btn btn-circle default btnCancel">Hủy</button>
					</div>
				</div>
			</div>
		</div>
		<!-- END FORM-->
	</div>
</div>