<?php 
$this->load->model('Categories');
$this->load->model('Toppings');
$categories = $this->Categories->getList()['records'];
$toppings = $this->Toppings->getList()['records'];

 ?>
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i>Thông tin chi tiết
			<a class="btn red btn-xs" href="<?php echo module_url($params->method . '/add') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
		</div>
		<div class="tools">
			<a href="#portlet-config" data-toggle="modal" class="config" title="Thiết đặt"></a>
			<a class="reload" title="Làm mới dữ liệu"></a>
			<a class="remove" title="Xóa"></a>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<div class="form-horizontal Form_<?php echo $action?>">
			<div class="form-body row">

				<div class="col-lg-8">
					<div class="form-group forEdit">
						<label class="col-md-4 control-label">ID</label>
						<div class="col-md-8">
							<span class="form-control-static idx"><?php echo @$row->id; ?></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Danh mục</label>
						<div class="col-md-8">
							<select class="form-control select2" name="category_id" required>
								<option></option>
								<?php foreach ($categories as $category): ?>
								<option value="<?php echo $category->id ?>" <?php if ($category->id == @$row->category_id) echo 'selected' ?>><?php echo $category->name ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Tên món</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="name" value="<?php echo @$row->name ?>" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Mã sản phẩm</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="product_id" value="<?php echo @$row->product_id ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Mô tả ngắn</label>
						<div class="col-md-8">
							<textarea class="form-control" name="short_description" cols="30" rows="5"><?php echo @$row->short_description ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Đơn giá</label>
						<div class="col-md-8">

							<div class="input-group">
								<span class="input-group-addon" style="width: 70px">
								Size 12oz
								</span>
								<input type="number" class="form-control" name="price" value="<?php echo @$row->price ?>" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon" style="width: 70px">
								Size 16oz
								</span>
								<input type="number" class="form-control" name="price_m" value="<?php echo @$row->price_m ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon" style="width: 70px">
								Size 22oz
								</span>
								<input type="number" class="form-control" name="price_l" value="<?php echo @$row->price_l ?>">
							</div>

						</div>
					</div>

					<div class="form-group">
						<?php $toppingSelecteds = explode(',', @$row->toppings); ?>
						<label class="col-md-4 control-label">Topping/Món thêm</label>
						<div class="col-md-8">
							<select name="toppings" class="form-control select2 tags" multiple="multiple">
							<?php foreach ($toppings as $item): ?>
								<option value="<?php echo $item->id ?>" <?php if (in_array($item->id, $toppingSelecteds)) echo 'selected' ?>><?php echo $item->name ?></option>
							<?php endforeach ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Số lượng</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="text" class="form-control" name="quantity" value="<?php echo @$row->quantity ?>">
								</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Trạng thái</label>
						<div class="col-md-8">
							<div class="md-checkbox has-success">
								<input type="checkbox" id="status" name="status" class="md-check"<?php if (@$row->status == 1 OR $action === 'add') echo ' checked' ?>>
								<label for="status"><span></span><span class="check"></span><span class="box"></span></label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Công thức</label>
						<div class="col-md-8">
						</div>
					</div>

				</div>

				<div class="col-lg-4">
					<span class="btn btn-sm btn-success btnUpload">
					    <i class="glyphicon glyphicon-upload"></i>
					    <span>Tải lên</span>
					</span>
					<input class="fileupload" type="file" name="files[]" data-unique="<?php echo md5(time() . randomString(10)) ?>">
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					<a class="thumbnail previewUpload"><?php echo getCoverImage($params->method, @$row->id) ?></a>
				</div>

			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button class="btn btn-circle blue btnUpdate">Cập nhật</button>
						<button class="btn btn-circle default btnCancel">Hủy</button>
					</div>
				</div>
			</div>
		</div>
		<!-- END FORM-->
	</div>
</div>