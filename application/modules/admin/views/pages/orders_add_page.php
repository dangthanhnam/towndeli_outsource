<?php
	$this->load->model('Menus');
	$this->load->model('Cities');
	$this->load->model('Districts');
	$this->load->model('Branches');
	$this->load->model('Users');

	$districts = $this->Districts->getList(['city_id' => 1])['records'];
	$menuGroup = $this->Menus->getAllWithGroup();
	$cities = $this->Cities->getList()['records'];
	$branches = $this->Branches->getList()['records'];


	if (!empty($row->customer_id)) {
		$user = $this->Users->getDetail($row->customer_id);
	}


	$menuSizes = [
	    12 => 'price',
	    16 => 'price_m',
	    22 => 'price_l'
	];
	krsort($menuSizes);

	$openHour = strtotime('09:00:00');
	$closeHour = strtotime('20:00:00');
	$nextTime = strtotime(date('H:00:00')) + ceil(date('i')/15)*15 * 60;
	if ($nextTime < $openHour) {
		$nextTime = $openHour;
	}
	$schedules = [];
	while ($nextTime < $closeHour) {
		$nextTime += 900;
		$schedules[] = $nextTime;
	}
?>

<div class="row">
	<div class="col-lg-8 col-md-7 col-sm-6 orderMenu">
		<?php foreach ($menuGroup as $group): ?>
		<div class="row orderMenuItems">
			<div>
				<strong><?php echo $group['category']->name; ?></strong>
			</div>
			<?php foreach ($group['menus']['records'] as $item):
				$sizes = [];
				foreach ($menuSizes as $sizeNum => $fieldSize) {
					$sizeValue = $item->{$fieldSize};
					if ($sizeValue > 0) {
						$sizes[] = '<a class="btn btn-xs btn-success" data-size="' . $fieldSize . '">' . $sizeValue/1000 . 'K</a>';
					}
				}
				$sizes = implode(' ', $sizes);
			?>
				<div data-id="<?php echo $item->id ?>" class="col-lg-4 col-md-6">
					<span><?php echo $item->name ?></span> <?php echo $sizes ?>
				</div>
			<?php endforeach; ?>
		</div>
		<?php endforeach; ?>
	</div>
	<div class="col-lg-4 col-md-5 col-sm-6 orderCustomer">
		<h3>Thông tin đơn hàng</h3>
		<div class="orderCartInfo">
			<?php $this->load->view('pages/orders_cart'); ?>
		</div>
	</div>
</div>


<div id="modal_addMenu" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Thêm món</h4>
      </div>
      <div class="modal-body">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
        <button type="button" class="btn btn-primary btn_addMenuSave">Lưu</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="orderCustomerInfo">

	<span class="idx hidden"><?php echo @$row->id ?></span>

	<a class="orderCustomerToggle btnCustomerInfoToggle">
		<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
	</a>
	<br />
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<label>T.Tin K.Hàng <span id="orderCustomerNotification"></span></label>
				<input type="hidden" id="orderCustomerId" name="customer_id" value="<?php echo @$row->customer_id ?>">

				<div class="form-horizontal">

					<div class="form-group">
						<label for="orderCustomerName" class="col-lg-2 col-md-2 control-label">KH</label>
						<div class="col-lg-10 col-md-10">
							<input type="text" class="form-control" id="orderCustomerName" name="customer_name" placeholder="Họ tên khách hàng" value="<?php echo @$user->firstname ?>" required="">
						</div>
					</div>

					<div class="form-group">
						<label for="orderCustomerPhone" class="col-lg-2 col-md-2 control-label">SĐT</label>
						<div class="col-lg-10 col-md-10">
							<input type="text" class="form-control" id="orderCustomerPhone" name="customer_phone" placeholder="Số ĐT khách hàng" value="<?php echo @$user->phone ?>" required="" style="color:red">
						</div>
					</div>

					<div class="form-group">
						<label for="orderCustomerEmail" class="col-lg-2 col-md-2 control-label">GT</label>
						<div class="col-lg-10 col-md-10">

							<input type="hidden" id="orderCustomerGender" name="customer_gender" value="<?php echo @$user->gender ?>" />
							<div class="md-radio-inline">
							<?php foreach ([1 => 'Nam', 2 => 'Nữ'] as $key => $label): ?>
								<div class="md-radio" data-value="<?php echo $key ?>">
									<input type="radio" id="orderCustomerGender<?php echo $key ?>" name="orderCustomerGender" class="md-radiobtn" <?php if ($key == @$user->gender) echo 'checked' ?>>
									<label for="orderCustomerGender<?php echo $key ?>">
									<span></span>
									<span class="check"></span>
									<span class="box"></span>
									<?php echo $label ?> </label>
								</div>
							<?php endforeach ?>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="orderBranch" class="col-lg-2 col-md-2 control-label">CN</label>
						<div class="col-lg-10 col-md-10">
							<select name="branch_id" id="orderBranch" class="form-control" required="">
								<option value="">Chọn chi nhánh</option>
								<?php foreach ($branches as $branch): ?>
								<option value="<?php echo $branch->id ?>" <?php if ($branch->id == @$row->branch_id) echo 'selected' ?>><?php echo $branch->name ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>

				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<label>Thông tin giao hàng</label>

				<div class="form-horizontal">

					<div id="orderCustomerAddresses"></div>

					<div class="form-group">
						<div class="col-lg-12">
							<input type="text" class="form-control" name="shipping_floor" placeholder="Phòng/lầu" value="<?php echo @$row->shipping_floor ?>">
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-12">
							<input type="text" class="form-control" id="orderCustomerAddress" name="shipping_address" placeholder="Địa chỉ" value="<?php echo @$row->shipping_address ?>">
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-6 col-md-6">
							<input type="text" name="shipping_ward" class="form-control" placeholder="Xã/Phường" value="<?php echo @$row->shipping_ward ?>">
						</label>
						<div class="col-lg-6 col-md-6">
							<select name="shipping_district" id="orderShippingDistrict" class="form-control">
								<option value="">-- Quận/Huyện --</option>
								<?php foreach ($districts as $district): ?>
								<option value="<?php echo $district->id ?>" <?php if ($district->id == @$row->shipping_district) echo 'selected' ?>><?php echo $district->name ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>

				</div>

				<!-- LẤY ĐỊA CHỈ GIAO HÀNG GẦN ĐÂY (LÀM TẠM 1 THỜI GIAN RỒI XÓA) -->
				<div class="orderLastShipping"></div>

			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 hidden">
				<label>Người nhận (nếu là biếu tặng)</label>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="orderReceiverName" class="col-lg-3 col-md-3 control-label">Họ tên</label>
						<div class="col-lg-9 col-md-9">
							<input type="text" class="form-control" id="orderReceiverName" name="receiver_name" placeholder="Họ tên người nhận">
						</div>
					</div>

					<div class="form-group">
						<label for="orderReceiverPhone" class="col-lg-3 col-md-3 control-label">Số ĐT</label>
						<div class="col-lg-9 col-md-9">
							<input type="text" class="form-control" id="orderReceiverPhone" name="receiver_phone" placeholder="Số ĐT người nhận">
						</div>
					</div>

					<div class="form-group">
						<label for="orderReceiverMessage" class="col-lg-3 col-md-3 control-label">Lời nhắn</label>
						<div class="col-lg-9 col-md-9">
							<textarea name="receiver_message" id="orderReceiverMessage" class="form-control" cols="30" rows="3" placeholder="Lời nhắn tới người nhận"></textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<label>Ghi chú giao hàng</label>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="orderShippingSchedule" class="col-lg-3 col-md-3 control-label">Giao lúc</label>
						<div class="col-lg-9 col-md-9">
							<select class="form-control" id="orderShippingSchedule" name="shipping_on">
								<option value="">Sớm nhất có thể</option>
								<?php foreach ($schedules as $schedule): $schedule = date('H:i', $schedule); ?>
								<option value="<?php echo $schedule ?>" <?php if ($schedule == @$row->shipping_on) echo 'selected' ?>><?php echo $schedule ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="orderNote" class="col-lg-3 col-md-3 control-label">Ghi chú</label>
						<div class="col-lg-9 col-md-9">
							<textarea name="note" id="orderNote" class="form-control" cols="30" rows="6"><?php echo @$row->note ?></textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
				<a class="btn btn-md btn-success btnOrderCreate"><span class="glyphicon glyphicon-send"></span> Tạo/Sửa Đ.Hàng</a>

				<div style="margin-top: 20px">
					<a class="btn btn-md btn-danger btnOrderClean"><span class="glyphicon glyphicon-trash"></span> HỦY</a>
				</div>
			</div>
		</div>
	</div>
</div>