<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Users extends REST_Controller {

    private $selector = 'id, username, email, phone, points, firstname, lastname, birthday, gender, status';


    function __construct()
    {
        parent::__construct();

        $this->load->model('Users');
    }


    public function index_get($id = 0)
    {
        $selector = [
            'fields' => $this->selector,
            'escape' => true
        ];
        if ($id == 0) {
            $options = [
                'selector' => $selector
            ];
            $response = [
                'data' => $this->Users->getList([], $options),
                'pagination' => []
            ];
        } else {
            $response = $this->Users->getDetail($id, $selector['fields'], $selector['escape']);
        }

        $this->set_response($response);
    }


    public function index_post()
    {
        $response = [
            'result' => $this->Users->create()
        ];

        $this->set_response($response);
    }


    public function index_put()
    {
        $response = [
            'result' => $this->Users->edit()
        ];

        $this->set_response($response);
    }


    public function index_delete()
    {
        $response = [
            'result' => $this->Users->remove()
        ];

        $this->set_response($response);
    }


    public function login_post()
    {
        $result = $this->Users->login();
        $response = [
            'result' => !empty($result),
            'data' => $result
        ];

        $this->set_response($response);
    }


    public function logout_post()
    {
        $result = $this->Users->logout();
        $response = [
            'result' => !empty($result),
            'data' => $result
        ];

        $this->set_response($response);
    }


    public function find_by_phone_post()
    {
        $phone = $this->input->post('phone', true);
        $user = $this->Users->getDetailByField('phone', $phone, $this->selector);

        if ($user) {
            $this->load->model('Orders');
            $field = 'shipping_address, shipping_floor, shipping_ward, shipping_district, shipping_fulladdress';

            $wheres = [
                'customer_id' => $user->id,
                'shipping_fulladdress !=' => ''
            ];

            $options = [
                'selector' => [
                    'fields' => $field,
                    'escape' => true
                ],
                'group_by' => ['shipping_address'],
                'order_by' => [
                    'id' => 'DESC'
                ]
            ];

            $user->addresses = $this->Orders->getList($wheres, $options)['records'];
            $user->points = $this->Users->getPoint($user->id);

            $user->last_shipping = $this->Orders->getLastShipping($user->id);
        }

        $response = [
            'result' => !empty($user),
            'data' => $user
        ];

        $this->set_response($response);
    }

}
