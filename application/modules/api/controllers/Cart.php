<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Cart extends REST_Controller {

    private $inputs;


    function __construct()
    {
        parent::__construct();

        $inputs = $this->input->post(null, true);
        if (empty($this->inputs)) {
            $inputs = $this->input->input_stream(null, true);
        }

        $this->load->library('Cart');
        $this->cart->product_name_rules .= "\"\'\/\&";
        $this->load->library('session');
        $this->load->model('Menus');
        $this->load->model('Toppings');

        $params = [
            'rowid' => '',
            'id' => 0,
            'size' => '',
            'qty' => 1,
            'options' => [],
            'discount_code' => ''
        ];

        foreach ($params as $key => $defaultValue) {
            if (isset($inputs[$key])) {
                $this->inputs[$key] = $inputs[$key];
            } else {
                $this->inputs[$key] = $defaultValue;
            }
        }
        $this->inputs['toppings'] = isset($this->inputs['options']['toppings']) ? $this->inputs['options']['toppings'] : [];
    }


    public function index_get()
    {
        $this->load->library('session');

        $response = [
            'total' => $this->cart->total(),
            'total_items' => $this->cart->total_items(),
            'contents' => $this->cart->contents(),
            'discount_code' => strval($this->session->userdata('discount_code'))
        ];

        $this->set_response($response);
    }


    public function index_post()
    {
        $options = $this->inputs['options'];

        $menu = $this->Menus->getDetail($this->inputs['id']);
        $price = (int) $menu->{$this->inputs['options']['size']};
        $price += $this->cartPrice($this->inputs['toppings']);

        if ($menu) {
            $data = array(
                    'id'      => intval($menu->id),
                    'qty'     => $this->inputs['qty'],
                    'price'   => $price,
                    'name'    => $menu->name,
                    'options' => $this->inputs['options']
            );
            $data['rowid'] = $this->cart->insert($data);

            $response = [
                'result' => true,
                'data' => $data
            ];
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }


    public function index_put()
    {
        $rowID = $this->inputs['rowid'];
        $options = $this->inputs['options'];
        $id = $this->inputs['id'];
        $contents = $this->cart->contents();

        if (isset($contents[$rowID])) {
            $data = [
                'rowid' => $rowID,
                'qty' => $this->inputs['qty']
            ];

            if ($id > 0) {
                $currentData = $contents[$rowID];
                $menu = $this->Menus->getDetail($id);
                $currentData['price'] = (int) $menu->{$options['size']};

                $data['price'] = $currentData['price'] + $this->cartPrice(@$options['toppings']);
                $data['options'] = $options;
            }

            $this->cart->update($data);

            $response = [
                'result' => true,
                'data' => $data
            ];
            $this->set_response($response);
        }
    }


    public function index_delete()
    {
        if ($this->inputs['rowid'] !== '') {
            $data = [
                'rowid' => $this->inputs['rowid'],
                'qty' => 0
            ];
            $this->cart->update($data);
        } else {
            $this->cart->destroy();
            $this->session->unset_userdata('discount_code');
        }

        $this->set_response(true);
    }


    public function discount_post()
    {
        $this->load->library('session');
        $code = $this->inputs['discount_code'];

        $response = [
            'result' => false,
            'message' => ''
        ];

        if ($code !== '') {
            $this->load->model('Discounts');

            $this->session->set_userdata('discount_code', $code);
            $res = $this->Discounts->check($code);
            if ($res > 0) {
                $response = [
                    'result' => true,
                    'message' => 'Code is valid'
                ];
            }
        } else {
            $this->session->unset_userdata('discount_code');
            $response = [
                'result' => true,
                'message' => 'Code is removed'
            ];
        }

        $this->set_response($response);
    }


    private function cartPrice($toppings)
    {
        if (empty($toppings)) {
            return 0;
        }

        $total = 0;
        foreach ($toppings as $toppingID => $toppingQty) {
            $topping = $this->Toppings->getDetail($toppingID);
            if ($topping) {
                $toppingQty = (int) $toppingQty;
                if ($toppingQty < 1) {
                    $toppingQty = 1;
                }
                $total += intval($topping->price) * $toppingQty;
            }
        }

        return $total;
    }

}
