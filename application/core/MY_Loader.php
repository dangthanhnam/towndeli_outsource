<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH . 'third_party/MX/Loader.php';

class MY_Loader extends MX_Loader {


	/** Load a module model **/
	public function model($model, $object_name = NULL, $db_conn = FALSE)
	{
		if (is_array($model)) {
			foreach ($model as $key => $value) {
				if (is_int($key)) {
					$value = self::_model($value);
					parent::model($value, $value, $db_conn);
				} else {
					$key = self::_model($key);
					parent::model($key, $value, $db_conn);
				}
			}
		} else {
			if (is_null($object_name)) {
				$object_name = $model;
			}
			$model = self::_model($model);
			parent::model($model, $object_name, $db_conn);
		}
	}


	private function _model($model)
	{
		return preg_match('#_model$#', $model) ? $model : $model . '_model';
	}
}