<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Input extends CI_Input {


	public function post_stream($index = null, $xss_clean = null)
	{
		$input = $this->post($index, $xss_clean);
		if (empty($input)) {
			$raw_input = json_decode($this->raw_input_stream, true);
			if (empty($raw_input)) {
				$raw_input = [];
			}

			if (empty($index)) {
				$input = $raw_input;
			} else {
				$input = empty($input[$index]) ? '' : $input[$index];
			}
		}

		return $input;
	}
}