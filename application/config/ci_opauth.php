<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['opauth_config'] = array(
                                'path' => '/auth/auth/login/', //example: /ci_opauth/auth/login/
                    			'callback_url' => '/auth/auth/authenticate/', //example: /ci_opauth/auth/authenticate/
                                'callback_transport' => 'post', //Codeigniter don't use native session
                                'security_salt' => 'flnsdndslnsdjnfjsdnfjsdfsd',
                                'debug' => true,
                                'Strategy' => array( //comment those you don't use
                                    // 'Twitter' => array(
                                    //     'key' => 'twitter_key',
                                    //     'secret' => 'twitter_secret'
                                    // ),
                                    'Facebook' => array(
                                        'app_id' => '465910000248243',
                                        'app_secret' => '3720f8dcdcf522b783ed351e91c7f356'
                                    ),
         //                            'Google' => array(
         //                                'client_id' => 'client_id',
         //                                'client_secret' => 'client_secret'
         //                            ),
         //                            'OpenID' => array(
									// 	'openid_url' => 'openid_url'
									// )
                                )
                            );

/* End of file ci_opauth.php */
/* Location: ./application/config/ci_opauth.php */
