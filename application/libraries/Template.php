<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include 'Stencil.php';

class Template extends Stencil {

	protected $CI;
	const LAYOUT_DEFAULT = 'default_layout';
	const VIEW_SUFFIX = '_page';


	function __construct()
	{
		parent::__construct();

		$this->CI =& get_instance();

		$this->CI->load->library('Slices');
	}


	public function paint($page = NULL, $data = array())
	{
		$module 	= $this->CI->router->fetch_module();
		$controller = $this->CI->router->fetch_class();
		$method 	= $this->CI->router->fetch_method();
		$data['body_class'] = $controller . ' ' . $method;

		$obj = new stdClass();
		$obj->module = $module;
		$obj->controller = $controller;
		$obj->method = $method;
		$data['params'] = $obj;

		// set layout default
		if ($this->layout === '') {
			$this->layout = self::LAYOUT_DEFAULT;
		}

		// set page default
		if (is_null($page)) {
			$page = $method;
		}

		if (!is_array($data)) {
			foreach ($data as $key => $value) {
				$this->data[$key] = $value;
			}
		}

		foreach ($this->slice as $key => $value)
		{
			if (is_numeric($key))
			{
				unset($this->slice[$key]);
				$key = $this->_slicePath($value);
				$this->slice[$key] = $value;
			}
		}

		$page .= self::VIEW_SUFFIX;
		parent::paint($page, $data);
	}


	private function _slicePath($slice)
	{
		return preg_replace('#/|-#', '_', $slice);
	}
}