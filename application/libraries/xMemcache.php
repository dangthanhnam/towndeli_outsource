<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class xMemcache extends Memcache {
	
	private $isConnected = FALSE;


	function __construct()
	{
		$CI =& get_instance();
		$CI->load->config('memcached', TRUE);
		$config = $CI->config->item('default', 'memcached');

		try {
			if (parent::addServer($config['hostname'], $config['port'])) {
				$this->isConnected = TRUE;
			}
		}
		catch (Exception $e) {
			throw new Exception('Memcache: Connect fail');
		}
	}


	public function set($name, $value, $expire = 0) {
		parent::set($name, $value, FALSE, $expire);
	}


	public function isConnected()
	{
		return $this->isConnected;
	}
}