<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Auth {

	private $ci;
	private $config;
	private $userId = null;


	function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('JWT');
		$this->config = $this->ci->load->config('auth', true);
	}


	public function getToken($user)
	{
		$payload = [
			'id' => (int) $user->id,
			'access_token' => $user->access_token
		];

		return $this->ci->jwt->encode($payload, $this->config['secret_key']);
	}


	public function getPayload()
	{
		$token = $this->ci->input->get_request_header('X-Auth-Token');

		if (is_null($token)) {
			return false;
		}

		try {
			return $this->ci->jwt->decode($token, $this->config['secret_key']);
		} catch (\Exception $e) {
			return false;
		}
	}
	

	public function userId()
	{
		if (isset($this->ci->session)) {
			$this->userId = (int) $this->ci->session->userdata('userId');
		} else {
			if (is_null($this->userId)) {
				$payload = $this->getPayload();
				if ($payload) {
					$this->userId = $payload->id;
				}
			}
		}

		return $this->userId;
	}
}