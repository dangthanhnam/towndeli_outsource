<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class xMongo extends MongoClient {
	
	private $dbname;
	private $isConnected = FALSE;


	function __construct()
	{
		$CI =& get_instance();
		$config = $CI->load->config('mongo', TRUE);

		$this->dbname = $config['dbname'];

		try {
			if (is_null(parent::__construct('mongodb://' . $config['user'] . ':' . $config['pass'] . '@' . $config['host']))) {
				$this->isConnected = TRUE;
			}
		}
		catch (Exception $e) {
			throw new Exception('Mongo: Connect fail');
		}
	}


	public function select($collection, $dbname = '')
	{
		if ($dbname === '') {
			$dbname = $this->dbname;
		}

		return parent::selectCollection($dbname, $collection);
	}


	public function isConnected()
	{
		return $this->isConnected;
	}
}