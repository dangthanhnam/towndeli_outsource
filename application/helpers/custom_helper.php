<?php

defined('BASEPATH') OR exit('No direct script access allowed');


function orderStatus($status)
{
	switch ($status) {
		
		case 0:
			return 'Đang chờ duyệt';
			break;

		case 1:
			return 'Đang xử lý';
			break;

		case 2:
			return 'Đang đi giao';
			break;

		case 3:
			return 'Đã giao';
			break;
		
		default:
			# code...
			break;
	}
}


function getCartContentToppings($toppings)
{
    if (empty($toppings) OR is_null($toppings)) {
        return '';
    }

    $CI =& get_instance();
    $toppingData = $CI->Toppings->getListByIDs(array_keys($toppings));

    $data = [];
    foreach ($toppingData as $row) {
        $data[] = $row->name . ' [' . $toppings[$row->id] . ']';
    }

    return implode(', ', $data);
}