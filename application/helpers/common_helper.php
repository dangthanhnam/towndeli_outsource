<?php

defined('BASEPATH') OR exit('No direct script access allowed');


function asset_url($path, $isAbsolute = true)
{
	$URI = '/assets/' . $path;
	return $isAbsolute ? site_url($URI) : $URI;
}


function module_url($path = '')
{
	$CI =& get_instance();
	$default_controller = explode('/', $CI->router->default_controller);
	$module = $CI->router->fetch_module();
	if ($default_controller[0] === $module) {
		$module = '';
	}

	return site_url($module . '/' . $path);
}


function get_domain($url = '')
{
	if ($url === '') {
		$CI =& get_instance();
		$CI->load->library('user_agent');
		$url = $CI->agent->referrer();
	}

	if ($url === '' OR filter_var($url, FILTER_VALIDATE_IP)) {
		return $url;
	}

	if (!preg_match('#^http(s)?://(.*)#', $url)) {
		$url = 'http://' . $url;
	}

	$p = strtolower(parse_url($url, PHP_URL_HOST));

	if (filter_var($p, FILTER_VALIDATE_IP)) {
		return $p;
	}

	$arr = array_slice(explode('.', $p), -3);

	$tlds = array(
		'tv',
		'co',
		'aero',
		'arpa',
		'asia',
		'biz',
		'cat',
		'com',
		'coop',
		'edu',
		'gov',
		'info',
		'jobs',
		'mil',
		'mobi',
		'museum',
		'name',
		'net',
		'org',
		'post',
		'pro',
		'tel',
		'travel',
		'xxx',
	);

	if (end($arr) === 'localhost') {
		return 'localhost';
	}

	if (count($arr) <= 2 OR in_array($arr[1], $tlds)) {
		return implode('.', $arr);
	}
	else {
		return $arr[1] . '.' . $arr[2];
	}

}


function get_host($url = '')
{
	if ($url === '') {
		$CI =& get_instance();
		$CI->load->library('user_agent');
		$url = $CI->agent->referrer();
	}
	
	if (!preg_match('#^http(s)?://(.*)#', $url)) {
		$url = 'http://' . $url;
	}

    return strtolower(strval(parse_url($url, PHP_URL_HOST)));
}


function compressHTML($str)
{
    $search = array(
        '/\>[^\S ]+/s', // strip whitespaces after tags, except space
        '/[^\S ]+\</s', // strip whitespaces before tags, except space
        '/(\s)+/s' // shorten multiple whitespace sequences
    );

    $replace = array(
        '>', '<', '\\1'
    );

    return preg_replace($search, $replace, $str);
}


function randomString($length = 6, $type = 'BOTH')
{
	$characters = array_merge(range('a', 'z'), range('A', 'Z'));
	$numbers = range(0, 9);
	$range = '';

	if ($type === 'ONLY_CHAR') {
		$range = $characters;
	} elseif ($type === 'ONLY_NUM') {
		$range = $numbers;
	} else {
		$range = array_merge($characters, $numbers);
	}
	$range = implode('', $range);

	$rangeLength = strlen($range);
	if ($length > $rangeLength) {
		$range = str_repeat($range, ceil($length/$rangeLength));
	}

	return substr(str_shuffle($range), 0, $length);
}


function genToken()
{
	return hash('sha256', implode('', array_values(func_get_args())) . TOKEN);
}


function prep_url($str = '')
{
	if ($str === 'http://' OR $str === '')
	{
		return '';
	}

	$url = parse_url($str);

	if ( ! $url OR ! isset($url['scheme']))
	{
		return 'http://'.$str;
	}

	return $str;
}


function slug($str, $lowercase = TRUE)
{
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'A', $str);
    $str = preg_replace('/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'E', $str);
    $str = preg_replace('/(Ì|Í|Ị|Ỉ|Ĩ)/', 'I', $str);
    $str = preg_replace('/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'O', $str);
    $str = preg_replace('/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'U', $str);
    $str = preg_replace('/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'Y', $str);
    $str = preg_replace('/(Đ)/', 'D', $str);

    $separator   = '-';
    $q_separator = preg_quote($separator);

    $trans = array(
        '&.+?;' => $separator, '[^a-z0-9 _-]' => $separator, '\s+' => $separator,
        '(' . $q_separator . ')+' => $separator
    );

    $str = strip_tags($str);

    foreach ($trans as $key => $val) {
        $str = preg_replace('#' . $key . '#i', $val, $str);
    }

    if ($lowercase === TRUE) {
        $str = strtolower($str);
    }

    $str = str_replace('_', '-', trim($str, $separator));

    return preg_replace('/--/', '-', $str);
}


function onePixel($header = true)
{
    if ($header) {
        header('Content-Type: image/gif');
        header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');
    }
    else {
        echo base64_decode('R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');
    }
}


function getPath($path) {
	str_replace(array('/', '\\'), DS, $path);
}


function trim_all($str = '')
{
	return preg_replace("/[[:blank:]]+/", ' ', trim($str));
}


function getFiles($path = '.')
{
	$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::SELF_FIRST);

	$files = array();
	foreach ($iterator as $file) {
	    $file = strval($file);
	    if (is_file($file)) {
	    	$files[] = str_replace('\\', '/', $file);
	    }
	}

	return $files;
}


function removeDirectory($path) {
	$path = trim($path, '/');
 	$files = glob($path . '/*');
	foreach ($files as $file) {
		is_dir($file) ? removeDirectory($file) : unlink($file);
	}
	@rmdir($path);

 	return;
}


function dump()
{
	$args = func_get_args();

	foreach ($args as $data) {
		echo '<pre>';
		var_dump($data);
		echo '</pre>';
	}
}


function dump_r()
{
	$args = func_get_args();

	foreach ($args as $data) {
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	}
}


function curl($path, $data = [], $method = '', $headers = [])
{
	$url = (strpos($path, '/') === 0) ? ($this->host . $path) : $path;
	$ch = curl_init();

	switch ($method) {
		case 'POST':
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		break;

		case 'PUT':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		break;

		case 'DELETE':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		break;

		default:
			if (!empty($data)) {
				$url .= (strpos($url, '?') ? '&' : '?') . http_build_query($data);
			}
		break;
	}

	if (empty($headers)) {
		$headers = ['Accept: application/json'];
	}

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, false);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	$result = json_decode(curl_exec($ch));
	curl_close($ch);

	return $result ? $result : null;
}


function getCoverImage($table = null, $id = null, $extension = 'png')
{
	$file = 'assets/upload/' . $table . '/' . $id . '/coverImage.' . $extension;

	return is_file($file) ? '<img class="img-responsive" src="' . site_url($file) . '" />' : '';
}


function getCoverImageLazyLoad($table, $id)
{
	$file = '/files/' . $table . '/' . $id . '/coverImage.png';
	$path = ROOT . '/public' . $file;
	$src = SITE . $file;

	return is_file($path) ? '<img class="img-responsive lazy" data-original="' . $src . '" />' : '';
}