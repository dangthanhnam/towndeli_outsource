<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Menus_model extends MY_Model {

    public $table = 'menus'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    private $sizes = [
                        'price' => '12oz',
                        'price_m' => '16oz',
                        'price_l' => '22oz'
    ];


    public function getAllWithGroup()
    {
        $this->load->model('Categories');
        $data = [];

        $categories = $this->Categories->getAllActive();

        foreach ($categories as $category) {
            $menus = $this->getList(['status' => 1, 'category_id' => $category->id]);
            $data[] = [
                'category' => $category,
                'menus' => $menus
            ];
        }

        return $data;
    }


    public function getFeatures($limitCategories, $limitMenuPerCategory)
    {
        $this->load->model('Categories');

        $categories = $this->Categories->getAvailable($limitCategories);

        $result = array();

        foreach ($categories as $category) {
            $menus = $this->db
                            ->from($this->table)
                            ->where('status', 1)
                            ->order_by('', 'RANDOM')
                            ->limit($limitMenuPerCategory)
                            ->get()
                            ->result();

            $result[] = array(
                    'category' => $category,
                    'menus' => $menus
                );
        }

        return $result;
    }


    public function getCombos($limit)
    {
        return $this->db
                        ->from($this->table)
                        ->where('status', 1)
                        ->where('is_combo', 0)
                        ->order_by('', 'RANDOM')
                        ->limit($limit)
                        ->get()
                        ->result();
    }


    public function getSizeOz($size)
    {
        return $this->sizes[$size];
    }
}