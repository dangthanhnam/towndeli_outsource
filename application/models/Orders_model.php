<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Orders_model extends MY_Model {

    public $table = 'orders'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $orderStatuses = [
        0 => 'Đang chờ',
        1 => 'Đang xử lý',
        2 => 'Đang giao',
        3 => 'Đã giao',
        4 => 'Hoàn tất',
        5 => 'Hủy'
    ];


    public function create($data = [], $extraData = [])
    {
        if (empty($data)) {
            $data = $this->assignData();
        }

        $this->load->library('Cart');
        $this->load->library('session');
        
        if ($this->cart->total_items() > 0 && !empty($data)) {

            $data = array_merge($data, $this->cartData($data));

            /* Auto create new customer account if not exist */
            if (empty($data['customer_id'])) {
                $this->load->model('Users');
                $posts = $this->input->post_stream(null, true);
                $chkUser = $this->Users->getDetailByField('phone', $posts['customer_phone']);
                if (empty($chkUser)) {
                    $userId = $this->Users->create([
                            'email' => strtolower(randomString()) . '_' . $posts['customer_phone'] . '@domain.com',
                            'phone' => $posts['customer_phone'],
                            'password' => randomString(),
                            'gender' => $posts['customer_gender'],
                            'firstname' => $posts['customer_name']
                        ]);
                } else {
                    $userId = $chkUser->id;
                }

                $data['customer_id'] = $userId;
            }

            // Get total order on today
            $lastOrderToday = $this->db
                                    ->select('order_number as no')
                                    ->where('DATE(`time_created`)', date('Y-m-d'))
                                    ->order_by('order_number', 'DESC')
                                    ->get($this->table)
                                    ->row();

            $data['order_number'] = is_null($lastOrderToday) ? 1 : intval($lastOrderToday->no) + 1;

            $data = $this->handlePoint($data);

            $orderId = parent::create($data);

            // Gửi SMS
            if ($orderId) {
                $this->load->model('Messages');
                $this->Messages->sendConfirmOrder('', $orderId);

                $this->cart->destroy();
                $this->session->unset_userdata('discount_code');
            }

            return $orderId;

        } else {
            return false;
        }
    }


    public function edit($data = [], $extraData = [])
    {
        if (empty($data)) {
            $data = $this->assignData();
        }

        $id = (int) $this->input->post_stream('id');

        $current = $this->db->where('id', $id)->get($this->table)->row();

        if (isset($data['status']) && $data['status'] == 4) {
            $this->load->model('Users');
            $this->Users->calculateLevel($current->customer_id);
        }

        $this->load->library('Cart');
        $this->load->library('session');

        $data = array_merge($data, $this->cartData($data));

        if (!empty($data)) {

            /* Auto create new customer account if not exist */
            if (empty($data['customer_id']) && !empty($posts['customer_phone'])) {
                $this->load->model('Users');
                $posts = $this->input->post_stream(null, true);
                $customerId = (int) $data['customer_id'];
                $chkUser = $this->Users->getDetailByField('phone', $posts['customer_phone']);
                if (empty($chkUser)) {
                    $userId = $this->Users->create([
                            'email' => strtolower(randomString()) . '_' . $posts['customer_phone'] . '@domain.com',
                            'phone' => $posts['customer_phone'],
                            'password' => randomString(),
                            'firstname' => $posts['customer_name'],
                            'gender' => empty($posts['customer_gender']) ? 'None' : $posts['customer_gender']
                        ]);
                } else {
                    $userId = $chkUser->id;
                    $this->Users->edit([
                        'id' => $userId,
                        // 'phone' => $posts['customer_phone'],
                        'firstname' => @$posts['customer_name'],
                        'gender' => empty($posts['customer_gender']) ? 'None' : $posts['customer_gender']
                    ]);
                }
            }

            $this->session->unset_userdata('discount_code');
            $this->cart->destroy();

            $data = $this->handlePoint($data);

            return parent::update($data, $id);

        } else {
            return false;
        }        
    }


    private function cartData($inpuData)
    {
        $outputData = [];
        $discountCode = $this->session->userdata('discount_code');

        if ($this->cart->total_items() > 0) {
            $outputData['cart_data'] = json_encode($this->cart->contents(), JSON_UNESCAPED_UNICODE);
            $outputData['cart_total_items'] = $this->cart->total_items();
            $outputData['cart_subtotal'] = $this->cart->total();
            if (!is_null($discountCode)) {
                $this->load->model('Discounts');
                $outputData['cart_total'] = $this->Discounts->apply($discountCode);
            } else {
                $outputData['cart_total'] = $this->cart->total();
            }
        }

        return $outputData;
    }


    public function getStatuses($step = 0)
    {
        return $step > 0 ? $this->orderStatuses[$step] : $this->orderStatuses;
    }


    public function getLastStatus()
    {
        $keys = array_keys($this->orderStatuses);
        
        return array_pop($keys);
    }


    public function cancel()
    {
        $id = $this->input->post_stream('id', true);

        return $this->update(['status' => 5], ['id' => $id, 'status <=' => 2]);

        return true;
    }


    public function printing()
    {
        return $this->db
                    ->where('id', $this->input->post_stream('id'))
                    ->set('print_count', 'print_count + 1', false)
                    ->update($this->table);
    }


    public function delay()
    {
        $orderId = (int) $this->input->post_stream('id');

        $order = $this->getDetail($orderId);

        if (empty($order->shipping_delay) && $order->status == 1) {
            $this->load->model('Messages');

            $this->update(['shipping_delay' => 15], $orderId);

            return $this->Messages->sendDelay('', $orderId);
        }


        return false;
    }


    private function handlePoint($data)
    {
        if ((!empty($data['cart_total']) OR !empty($data['point_used'])) && !empty($data['customer_id'])) {
            $this->load->model('Users');
            $userId = $data['customer_id'];
            $currentPoint = $this->Users->getPoint($userId);
            $pointUsed = empty($data['point_used']) ? 0 : abs(intval($data['point_used']));
            $charge = intval($data['cart_total']);
            $total2Point = ceil($charge/1000);

            if ($pointUsed >= $currentPoint) {
                $pointUsed = $currentPoint;
            }

            if ($pointUsed >= $total2Point) {
                $pointUsed = $total2Point;
                $charge = 0;
            } else {
                $charge -= $pointUsed*1000;
            }

            $data['points'] = $this->calculatePoint($userId, $charge);
            $data['point_used'] = $pointUsed;
            $data['cart_total'] = $charge;
        }

        return $data;
    }


    public function calculatePoint($userId, $amount)
    {
        $user = $this->db
                        ->select('level')
                        ->where('id', $userId)
                        ->get('users')
                        ->row();
                        
        if (is_null($user)) {
            return 0;
        }

        $level = (int) $user->level;
        $point = 0;

        if ($level <= 2) {
            $point = floor($amount/30000);
        } elseif ($level === 3) {
            $point = floor($amount/25000);
        } else {
            $point = floor($amount/20000);
        }

        return intval($point);
    }


    public function getLastShipping($userId)
    {
        $data = [];
        $res = $this->db
            ->select('address')
            ->where('customer_id', $userId)
            ->group_by('address')
            ->order_by('id', 'DESC')
            ->get('orders')
            ->result();

        if (!empty($res)) {
            foreach ($res as $row) {
                $data[] = $row->address;
            }
        }

        return $data;
    }
}
