<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Report_model extends MY_Model {


    public function total()
    {
        $data = [];
        $objs = ['branches', 'categories', 'menus', 'toppings', 'users', 'orders'];
        foreach ($objs as $key => $value) {
            $data[$value] = $this->db->count_all_results($value);
        }

        return $data;
    }


    public function totalByDate($range)
    {
        $this->load->model('Orders');

        $wheres = [];
        if (!is_null($range)) {
            $wheres = ['DATE(time_created)' => $range];
        }

        $orders = [];
        $orderStatuses = $this->Orders->orderStatuses;
        foreach ($orderStatuses as $key => $value) {
            $orders[$value] = $this->db->where($wheres)->where('status', $key)->count_all_results('orders');
        }

        $shippers = $this->db->select('id, firstname, lastname, phone')->where('role', 4)->get('users')->result();
        foreach ($shippers as $key => $row) {
            $res = $this->db->select('count(id) as count, sum(cart_total_items) as total_item, sum(cart_total) as total', false)->where($wheres)->where('shipper_id', $row->id)->where('status', 4)->get('orders')->row();
            $row->order_count = (int) $res->count;
            $row->total_item = (int) $res->total_item;
            $row->order_total = (int) $res->total;
            $shippers[$key] = $row;
        }

        $total = $this->db->select('count(id) as count, sum(cart_total_items) as total_item, sum(cart_total) as total', false)->where($wheres)->where('status', 4)->get('orders')->row();

        return [
            'orders' => $orders,
            'orderFinish' => (int) $total->count,
            'orderItem' => (int) $total->total_item,
            'revenue' => (int) $total->total,
            'shippers' => $shippers
        ];
    }
}