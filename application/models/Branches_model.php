<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Branches_model extends MY_Model {

    public $table = 'branches'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update


    public function getList($wheres = [], $options = [])
    {
        $this->db->start_cache();
        $this->db
                ->select($this->table . '.*, c.name as c_name, d.name as d_name')
                ->join('districts as d', 'd.id = ' . $this->table . '.location_id')
                ->join('cities as c', 'c.id = d.city_id');
    	
        return parent::getList($wheres = [], $options = []);
    }
}