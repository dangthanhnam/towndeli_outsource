<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Categories_model extends MY_Model {

    public $table = 'categories'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $options = [
        'order_by' => [
            'name' => 'ASC'
        ]
    ];


    public function getAvailable($limit = NULL)
    {
    	$res = $this->db
    				->select($this->table . '.*, count(menus.id) as count')
    				->join('menus', 'menus.category_id = ' . $this->table . '.id')
    				->group_by('category_id')
    				->having('count >', 0)
    				->order_by('count', 'DESC')
    				->limit($limit)
    				->get($this->table)
                    ->result();

		return $res;
    }
}