<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Discounts_model extends MY_Model {

    public $table = 'discounts'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update


    public function getAvailable()
    {
        return $this->db
                    ->where('type', 2)
                    ->where('time_start <= NOW()', null, false)
                    ->where('time_end >= NOW()', null, false)
                    ->where('status', 1)
                    ->get($this->table)
                    ->result();
    }


    public function apply($code, $isSave = true)
    {
        $code = strtolower($code);
        $this->load->model('Menus');
        $this->load->library('Cart');
        $total = $this->cart->total();

        // Check discount code
        $row = $this->getDetailByField('code', $code);
        if (is_null($row)) {
            return 0;
        }


        // Check discount type
        if ($row->type == 1) {
            $check = $this->db
                            ->where('code', $code)
                            ->count_all_results('discounts_used');

            if ($check > 0) {
                return 0;
            }
        }


        $discount = 0;
        switch ($row->type_value) {

            // Giảm giá theo số tiền
            case 1:
                $discount = (int) $row->value;
            break;

            // Giảm giá theo phần trăm
            case 2:
                $discount = round($total * intval($row->value)/100);
            break;

            // Giảm số theo chương trình
            case 3:
                $moduleName = 'module_' . $row->code;
                if (method_exists($this, $moduleName)) {
                    $discount = $this->{$moduleName}();
                } else {
                    $discount = 0;
                }
            break;
        }

        if ($isSave) {
            $discounts_used = [
                'code' => $code,
                'discount_id' => $row->id,
                'user_id' => 1,
                'amount' => $discount,
                'time_add' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('discounts_used', $discounts_used);
        }


        return $total > 0 ? $total - $discount : 0;
    }


    public function check($code)
    {
        return $this->apply($code, false);
    }


    // Chương trình combo 4 ly
    private function module_combo4()
    {
        if ($this->cart->total_items() < 4) {
            return 0;
        }
        
        $comboPrice = 99000;
        $comboSize = 4;

        $price16oz = [];
        foreach ($this->cart->contents() as $rowID => $values) {
            $size = $values['options']['size'];
            if ($size === 'price_m') {
                $menu = $this->Menus->getDetail($values['id']);
                $price = (int) $menu->{$size};
                $qty = (int) $values['qty'];
                for ($i = 1; $i <= $qty; $i++) {
                    $price16oz[] = $price;
                }
            }

        }
        rsort($price16oz);

        $n = floor(count($price16oz)/$comboSize);
        $comboSizeApply = $comboSize * $n;
        $comboTotal = array_sum(array_slice($price16oz, 0, $comboSizeApply));
        $combosPrice = $comboPrice * $n;
        
        if ($comboTotal > $combosPrice) {
            return $comboTotal - $combosPrice;
        } else {
            return 0;
        }
    }


    private function module_big33()
    {
        $min = 3;
        $count = $this->cart->total_items();
        if ($count < $min) {
            return 0;
        }

        $count22oz = 0;
        $total = 0;
        foreach ($this->cart->contents() as $rowID => $values) {
            $size = $values['options']['size'];
            if ($size === 'price_l') {
                $menu = $this->Menus->getDetail($values['id']);
                $price = (int) $values['price'];
                $qty = intval($values['qty']);
                $total += $price*$qty;
                $count22oz += $qty;
            }
        }

        if ($count22oz >= $min) {
            return round($total * 0.33, -3);
        } else {
            return 0;
        }
    }
}