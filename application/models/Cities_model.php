<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Cities_model extends MY_Model {

    public $table = 'cities'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update


    public function getListDistrict($cityID = 0)
    {
        if ($cityID === 0) {
            $cityID = $this->input->post('city_id');
        }

        return $this->db->where('city_id', $cityID)->get('districts')->result();
    }


    public function getCityByDistrict($districtID = 0)
    {
    	$result = $this->db
    					->select('t.id')
    					->from($this->table . ' as t')
						->join('districts as d', 'd.city_id = t.id')
    					->where('d.id', $districtID)
    					->get()
    					->result();

		return empty($result) ? 0 : (int) $result[0]->id;
    }
}