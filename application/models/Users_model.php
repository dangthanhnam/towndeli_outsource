<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Users_model extends MY_Model {

    public $table = 'users'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $roles = [
        1 => 'Admin',
        2 => 'Tổng đài viên',
        3 => 'Quản lý chi nhánh',
        4 => 'Nhân viên',
        9 => 'Khách hàng'
    ];


    public function create($data = [], $extraData = [])
    {
        return parent::create($data, $this->getExtraData());
    }


    public function edit($data = [], $extraData = [])
    {
        $extraData = $this->getExtraData();
        
        if (!empty($extraData['password'])) {
            $userId = $this->input->post('id');
            $currentPassword = $this->input->post('current_password');

            $checkPassword = $this->db
                                    ->where('id', $userId)
                                    ->where('`password` = sha1(concat(' . $currentPassword . ',salt))', NULL, FALSE)
                                    ->count_all_results($this->table);

            if ($checkPassword === 0) {
                return false;
            }
        }


        return parent::edit($data, $extraData);
    }


    private function getExtraData()
    {
        $inputs = $this->input->post_stream();

        if (isset($inputs['email'])) {
            $inputs['email'] = strtolower($inputs['email']);
        }

        if (!empty($inputs['password'])) {
            $salt = randomString(6);
            $extraData = [
                'salt' => $salt,
                'password' => sha1($inputs['password'] . $salt)
            ];
        } else {
            $extraData = [];
        }


        return $extraData;
    }


    public function calculateLevel($userId, $upgrade = true)
    {
        $res = $this->db
                    ->select_sum('points')
                    ->where('customer_id', $userId)
                    ->where('status', 4)
                    ->get('orders')
                    ->row();

        $point = (int) $res->points;

        if ($point <= 50) {
            $level = 1;
        } elseif ($point <= 700) {
            $level = 2;
        } elseif ($point <= 2000) {
            $level = 3;
        } else {
            $level = 4;
        }

        if ($upgrade) {
            $user = $this->db
                        ->select('level')
                        ->where('id', $userId)
                        ->get($this->table)
                        ->row();
            $currentLevel = (int) $user->level;

            if ($currentLevel < $level) {
                $this->update(['level' => $level], $userId);
            }
        }

        return $level;
    }


    public function getPoint($userId = null)
    {
        if (is_null($userId )) {
            $userId = $this->auth->userId();
        }
        
        $res = $this->db
                    ->select('(sum(points) - sum(point_used)) as point', false)
                    ->where('customer_id', $userId)
                    ->where('status <=', 4)
                    ->get('orders')
                    ->row();

        if (empty($res)) {
            return 0;
        } else {
            return (int) $res->point;
        }
    }


    public function getRoles($role = null)
    {
        if (is_null($role)) {
            return $this->roles;
        } else {
            return isset($this->roles[$role]) ? $this->roles[$role] : null;
        }
    }


    public function getEmployees()
    {
        $result = $this->get_all(['role' => 4]);

        return $result ? $result : [];
    }
    

    public function login()
    {
        $username = $this->db->escape_str(trim($this->input->post('username', true)));
        $password = $this->db->escape_str(trim($this->input->post('password')));

        if ($username == '' OR $password == '') {
            return false;
        }

        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $user = $this->db
                ->select('id')
                ->where($field, $username)
                ->where('`password` = sha1(concat("' . $password . '",`salt`))', NULL, FALSE)
                ->get($this->table)
                ->row();

        if (!empty($user)) {
            $this->session->set_userdata('userId', $user->id);
        }


        return $user;
    }


    public function logout()
    {
        $this->session->unset_userdata('userId');

        return true;
    }
}