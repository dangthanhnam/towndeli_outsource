<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class City_model extends MY_Model {

    public $table = 'cities'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    

    public function __construct()
    {
        parent::__construct();
    }


    public function getAllActive()
    {
    	return $this->where('status', 1)->get_all();
    }


    // Get list
    public function getList($wheres = [])
    {
        if (!empty($wheres)) {
            $this->db->where($wheres);
        }

        return $this->db
                    ->get($this->table)
                    ->result();
    }


    public function getDetailByField($field, $search)
    {
        $res = $this->getList([$this->table . '.' . $field => $search]);

        return $res ? $res[0] : NULL;
    }


    public function getDetail($id)
    {
        return $this->getDetailByField('id', $id);
    }
}