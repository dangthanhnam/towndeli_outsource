jQuery(document).ready(function($){

    $.ajaxSetup({
        url: URL.ajax,
        type: 'POST',
    });


    $(this).on('click', '#cart-item >a >i', function(){
        $("#cart-item").hide();
        $('#bg_wrap').hide();
    });


    $(this).on('keyup', function(e) {
        /* Act on the event */
        if (e.keyCode === 27) {
            $('.exit').trigger('click');
            $('#cart-item >a >i').trigger('click');
        }
    });


    // ĐĂNG KÝ TÀI KHOẢN
    $(this).on('click', '.btnUserSignUp', function(e){
        e.preventDefault();
        var data = getForm('.formUserRegister', 'create', 'users', true);

        if (data === false || data === null) {
            alert('Vui lòng nhập đủ dữ liệu!');
            return false;
        }
        console.log(data);

        $.ajax({
            url: API.users,
            data: data,
            success: function(res) {
                if (res.result === true) {
                    alert('Đã đăng ký thành công!');
                    window.location.href = window.location.href;
                }
            }
        });
    });


    // ĐĂNG NHẬP TÀI KHOẢN
    $(this).on('click', '.btnUserSignIn', function(e){
        e.preventDefault();
        var data = getForm('.formUserSignIn', 'login', 'users', true);

        if (data === false || data === null) {
            alert('Vui lòng nhập đủ dữ liệu!');
            return false;
        }

        $.ajax({
            url: API.users + '/login',
            data: data,
            success: function(res) {
                if (res.result === true) {
                    window.location.href = window.location.href;
                }
            }
        });
    });


    // Add topppings
    $(this).on('change', '#pick_topping', function(){
        var selected = $(this).children('option:selected');
        if (selected.val() !== '') {
            var id = selected.val();
            var name = selected.text();
            var price = selected.data('price');

            $('.toppingSelecteds').append('<span data-id="' + id + '" data-price="' + price + '">' + name + ' <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span>');
        }

        cartTotal();
    });

    // Remove topping
    $(this).on('click', '.toppingSelecteds >span >span', function(){
        $(this).parent().remove();
        cartTotal();
    });

    // Cart: Increase quantity
    $(this).on('click', '.cart_quantity .glyphicon-plus', function(){
        var rowid = $(this).closest('[data-id]').data('id');
        var qtyObj = $(this).next();
        var qty = parseInt(qtyObj.text()) + 1;
        qtyObj.text(qty);

        cartUpdate({
            rowid: rowid,
            qty: qty
        }, function(){
            cartTotalCheckout();
        });
    });
    // Cart: Decrease quantity
    $(this).on('click', '.cart_quantity .glyphicon-minus', function(){
        var rowid = $(this).closest('[data-id]').data('id');
        var qtyObj = $(this).prev();
        var qty = parseInt(qtyObj.text());
        if (qty > 1) {
            qty--;
        } else {
            qty = 1;
        }
        qtyObj.text(qty);

        cartUpdate({
            rowid: rowid,
            qty: qty
        }, function(){
            cartTotalCheckout();
        });
    });


    // Cart: remove item
    $(this).on('click', '.cart_btn_remove', function(){
        if (!confirm('Bạn muốn bỏ món này?')) {
            return false;
        }
        var row = $(this).closest('[data-id]');
        var rowID = row.data('id');
        var data = {
            rowid: rowID,
            qty: 0
        };
        cartUpdate(data, function(){
            row.remove();
        });
    });


    $(this).on('change', '#menu_qty', function(){
        cartTotal();
    });



    // CHECKOUT
    $(this).on('click', '.cartCheckout .btnCheckout', function(){
        var data = {};
        var chk_required = false;
        $('.cartCheckout [name]').each(function(){
            // console.log($(this).attr('name') + ': ' + $(this).val());
            if ($(this).val() != '') {
                data[$(this).attr('name')] = $(this).val();
            } else {
                if ($(this).is('[required]')) {
                    chk_required = true;
                    return false;
                }
            }
        });

        if ($('.cbox_gift').is(':checked')) {
            if ($('[name="customer_name"]').val() === '' || $('[name="customer_phone"]').val() === '') {
                chk_required = true;
            }
        }

        if (chk_required) {
            alert('Vui lòng nhập đủ thông tin!');
            return false;
        }

        data.useClass = 'orders';
        data.useAction = 'create';

        $.ajax({
            url: '/home/ajax',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(res) {
                // window.location.href = window.location.href;
            }
        });
    });


    //use select2
    $('.select2').select2({
        minimumResultsForSearch: Infinity
    });
    
    //feedback 
    $('.list-feedback').owlCarousel({
        autoplay:true,
        loop:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:false
            }
        }
    });
    //popup
    $("#color-red").click(function(){
        $("#popup-home-page").addClass("color-click-red").removeClass('color-click-green').removeClass('color-click-blue');
    });
    
    $("#color-blue").click(function(){
        $("#popup-home-page").addClass("color-click-blue").removeClass('color-click-red').removeClass('color-click-green');
    });
    
    $("#color-green").click(function(){
        $("#popup-home-page").addClass("color-click-green").removeClass('color-click-blue').removeClass('color-click-red');
    });
    
    $(this).on('click', '.exit', function(){
        $("#bg_wrap").hide(300);
        $("#popup-home-page").hide(300);
    });
    // cart hide
//   $("#main-content").click(function(){
//       $("#cart-item").hide(300);
//   });
   // end cart hide
    $(".option p").click(function(){
//        $(".bg-popup").show();
        var menu_id = $(this).closest('[data-id]').data('id');
        var size = $(this).data('size');
        $.ajax({
            url: '/ajax/menu',
            type: 'POST',
            data: {id: menu_id, size: size},
            success: function(res) {
                $('#popup-home-page').html(res);

                $("#bg_wrap").show();
                $("#popup-home-page").fadeIn(300);
                $("#cat_popup").hide();
                $("#menu-lang").hide();
            }
        });
    });

    // Select menu size
    $(this).on('click', '.menu_size >a', function(){
        $(this).siblings('a').removeClass('selected');
        $(this).addClass('selected');

        cartTotal();
    });

    //menu
    $(".categories-menu > i").click(function(){
        $("#cat_popup").toggle(300);
        $("#menu-lang").hide();
    });
    //cart
    $(".cart").click(function(){
//        $(".wrap-cart").toggle();

        var cart_total_items = parseInt($('#cart-item').data('total-items'));
        if (cart_total_items === 0) {
            return false;
        }

        $("#cart-item").slideDown(400);
        $("#bg_wrap").show();
        $("#menu-lang").hide();
        $("#cat_popup").hide();
    });
    $(".avatar").click(function(e){
        if ($('.wrap-login').length === 1) {
            e.preventDefault();
            $(".wrap-register").hide();
            $("#bg_wrap").show();
            $("#form-login").slideDown(400);
            $("#menu-lang").hide();
            $("#cat_popup").hide();
        }
    });
    $("#click-register").click(function(){
        $("#form-login").hide();
        $("#form-register").slideToggle(400);
        $("#menu-lang").hide();
        $("#cat_popup").hide();
    });
    $("#click-login").click(function(){
        $("#form-register").hide();
        $("#form-login").slideToggle(400);
        $("#menu-lang").hide();
        $("#cart-item").hide();
        $("#cat_popup").hide();
    });
    //language
    $(".lang > a").click(function(){
        $("#menu-lang").slideToggle(200);
        $("#cat_popup").hide();
    });
     //scroll menu
    var sticky = $('.header-bottom'),
          scrollNav = sticky.offset().top;
    $(window).on('scroll', function(){
        if ($(window).scrollTop() > scrollNav){
            $('#cat_popup').addClass('categoryFix');
            $('#menu-lang').addClass('langFix');
            $('#header-menu').addClass('is-fixed');
        }else{
            $('#cat_popup').removeClass('categoryFix');
            $('#header-menu').removeClass('is-fixed');
            $('#menu-lang').removeClass('langFix');
        }
    });
    //date
    $( "#datepicker" ).datepicker();
     $( "#datepicker" ).click(function(){
        $(".bg-popup").hide();
        $("#menu-lang").hide();
        $("#cat_popup").hide();
     })
    $(".fa-heart-o").click(function(){
        $(".plus").addClass("plus-heart");
    });    
    $("#popup-click-show").click(function(){
//        $(".bg-popup").show();
        $("#popup-home-page").fadeIn(300);
        $("#menu-lang").hide();
        $("#cat_popup").hide();
    });
    //smoothly
    $('.main-menu li.schedule a, .main-menu li.login-menu a').bind('click',function(event){
        var $anchor = $(this);
        var scrollToObj = $($anchor.attr('href'));
        if (scrollToObj.length === 0) {
            return;
        }
        $("#menu-lang").hide();
        $("#cat_popup").hide();
        
        var eleHeight = 0;
        if( !$('#header-menu').hasClass('is-fixed') ){
            eleHeight = 158;
        }else{
            eleHeight = 70;
        }
        
        $('html, body').stop().animate({
            scrollTop: scrollToObj.offset().top - eleHeight
        }, 1000,'easeInOutExpo');
        
        event.preventDefault();
    });

    //checkbox
    $('.delitm-checkbox label').click(function(){
        $('.delitm-checkbox label span').fadeToggle(100);
    });
    var removeSuccess;
    removeSuccess = function () {
        $('.plus .wrap').removeClass('success');
    };


    // Add to cart
    $(this).on('click', '.plus .button', function () {
        //$('.plus .wrap').addClass('success');
        //setTimeout(removeSuccess, 1500);
        var menu_id = $(this).closest('[data-id]').data('id');
        var size = $('.menu_size a.selected').data('field');
        var quantity = parseInt($('#menu_qty').val());
        var toppingData = {};
        var toppings = $('.toppingSelecteds >span');
        if (toppings.length > 0) {
            $.each(toppings, function(i, val){
                var t_id = $(this).data('id');
                var t_price = $(this).data('price');
                if (typeof toppingData[t_id] === 'undefined') {
                    toppingData[t_id] = 1;
                } else {
                    toppingData[t_id]++;
                }
            });
        }

        $.ajax({
            url: '/ajax/cart',
            type: 'POST',
            data: {
                id: menu_id,
                qty: quantity,
                options: {size: size, toppings: toppingData}
            },
            success: function(res) {
                $('.exit').trigger('click');
                getCartContents();
            }
        });
    });


    //selectbox
    $('#calende .select2').click(function(){
        $('li.select2-results__options').toggle(300);
    });
    //checkboxlogin
    $('.bottom .check-login').click(function(){
       $('.bottom .check-login label i').fadeToggle(100);
    });
    $('#form-register > a > i').click(function(){
        $('#form-register').hide(300);
        $('#bg_wrap').hide();
    });
    $('#form-login > a > i').click(function(){
        $('#bg_wrap').hide(300);
        $('#form-login').hide(300);
    });
    $('#cart-item > a > i').click(function(){
        $('#cart-item').hide(300);
        $('#bg_wrap').hide(300);
    });
    $('.formRight .left > span > label').click(function(){
        $('.formRight .left > span > label > i ').fadeToggle(200);
        $('.showForm').toggle(300);
    })
    $('#main-content').click(function(){
        $("#menu-lang").hide();
        $("#cat_popup").hide();
        $('#menu-schedule').hide();
    });
    $("#bg_wrap").click(function(){
        $('#cart-item').hide(300);
        $('#form-register').hide(300);
        $('#form-login').hide(300);
        $(this).hide(300);
        $('#popup-home-page').hide(300);
    });
    if($(window).width() < 667){
            $('.main-menu li.schedule a').bind('click',function(event){
            var $anchor = $(this);
            var eleHeight = 0;
            if( !$('#header-menu').hasClass('is-fixed') ){
                eleHeight = 128;
            }else{
                eleHeight = 70;
            }

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - eleHeight
            }, 1000,'easeInOutExpo');

            event.preventDefault();
        });
        $('.main-menu li.login-menu a').bind('click',function(event){
            var $anchor = $(this);

            var eleHeight = 0;
            if( !$('#header-menu').hasClass('is-fixed') ){
                eleHeight = 128;
            }else{
                eleHeight = 68;
            }

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - eleHeight
            }, 1000,'easeInOutExpo');

            event.preventDefault();
        });
    }
    if($(window).width() < 480){
            $('.main-menu li.schedule a').bind('click',function(event){
            var $anchor = $(this);
            var eleHeight = 0;
            if( !$('#header-menu').hasClass('is-fixed') ){
                eleHeight = 103;
            }else{
                eleHeight = 55;
            }

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - eleHeight
            }, 1000,'easeInOutExpo');

            event.preventDefault();
        });
        $('.main-menu li.login-menu a').bind('click',function(event){
            var $anchor = $(this);

            var eleHeight = 0;
            if( !$('#header-menu').hasClass('is-fixed') ){
                eleHeight = 103;
            }else{
                eleHeight = 55;
            }

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - eleHeight
            }, 1000,'easeInOutExpo');

            event.preventDefault();
        });
    }
    //SUBSCRIBE
    var addSubscribe;
    addSubscribe = function () {
        $('.wrapSub').addClass('subScrice');
    };
    $( window ).load(function() {
        setTimeout(addSubscribe, 3000);
    });
    $('.wrapSub').click(function(){
        $('.wrapSub').hide();
    }); 
});


function cartTotal()
{
    var menu_price = parseInt($('.menu_size a.selected').data('price'));
    var menu_qty = parseInt($('#menu_qty').val());
    var toppings = $('.toppingSelecteds >span');
    if (toppings.length > 0) {
        $.each(toppings, function(i, val){
            menu_price += parseInt($(this).data('price'));
        });
    }

    var total = menu_price * menu_qty;

    $('.cart_total').text(numberWithCommas(total) + ' đ');
}


function cartTotalCheckout()
{
    var subtotal = 0;
    $('.myCart tr').each(function(){
        var price = parseInt($(this).data('price'));
        var qty = parseInt($(this).find('.cart_quantity span').text());
        var _total = price * qty;
        subtotal += _total;
        $(this).find('.cart_subtotal').text(numberWithCommas(_total));        
    });

    $('.cartSubTotal').text(numberWithCommas(subtotal));
}


function getCartContents()
{
    $.ajax({
        url: 'ajax/get_cart_contents',
        success: function(res) {
            $('.wrap-cart').html(res);
            var total_items = $('#cart-item').data('total-items');
            $('.cart .count').text(total_items);
        }
    });
}


function cartUpdate(data, callback)
{
    if (data.qty == 0) {
        data.action = 'remove';
    } else {
        data.action = 'update';
    }

    $.ajax({
        url: '/ajax/cart',
        type: 'POST',
        data: data,
        success: function(res) {
            cartTotalCheckout();
            getCartContents();
            callback();
        }
    });
}


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}