jQuery(document).ready(function($){
    //use select2
    $('.select2').select2({
        minimumResultsForSearch: Infinity
    });
    
    //feedback 
    $('.list-feedback').owlCarousel({
        autoplay:true,
        loop:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:false
            }
        }
    });
    //popup
    $("#color-red").click(function(){
        $("#popup-home-page").addClass("color-click-red").removeClass('color-click-green').removeClass('color-click-blue');
    });
    
    $("#color-blue").click(function(){
        $("#popup-home-page").addClass("color-click-blue").removeClass('color-click-red').removeClass('color-click-green');
    });
    
    $("#color-green").click(function(){
        $("#popup-home-page").addClass("color-click-green").removeClass('color-click-blue').removeClass('color-click-red');
    });
    
    $(".exit").click(function(){
        $("#bg_wrap").hide(300);
        $("#popup-home-page").hide(300);
    });
    // cart hide
//   $("#main-content").click(function(){
//       $("#cart-item").hide(300);
//   });
   // end cart hide
    $(".option").click(function(){
//        $(".bg-popup").show();
        $("#bg_wrap").show();
        $("#popup-home-page").fadeIn(300);
        $("#cat_popup").hide();
        $("#menu-lang").hide();
    });
    //menu
    $(".categories-menu > i").click(function(){
        $("#cat_popup").toggle(300);
        $("#menu-lang").hide();
    });
    //cart
    $(".cart").click(function(){
//        $(".wrap-cart").toggle();
        $("#cart-item").slideDown(400);
        $("#bg_wrap").show();
        $("#menu-lang").hide();
        $("#cat_popup").hide();
    });
    $(".avatar").click(function(){
        $(".wrap-register").hide();
        $("#bg_wrap").show();;
        $("#form-login").slideDown(400);
        $("#menu-lang").hide();
        $("#cat_popup").hide();
    });
    $("#click-register").click(function(){
        $("#form-login").hide();
        $("#form-register").slideToggle(400);
        $("#menu-lang").hide();
        $("#cat_popup").hide();
    });
    $("#click-login").click(function(){
        $("#form-register").hide();
        $("#form-login").slideToggle(400);
        $("#menu-lang").hide();
        $("#cart-item").hide();
        $("#cat_popup").hide();
    });
    //language
    $(".lang > a").click(function(){
        $("#menu-lang").slideToggle(200);
        $("#cat_popup").hide();
    });
     //scroll menu
    var sticky = $('.header-bottom'),
          scrollNav = sticky.offset().top;
    $(window).on('scroll', function(){
        if ($(window).scrollTop() > scrollNav){
            $('#cat_popup').addClass('categoryFix');
            $('#menu-lang').addClass('langFix');
            $('#header-menu').addClass('is-fixed');
        }else{
            $('#cat_popup').removeClass('categoryFix');
            $('#header-menu').removeClass('is-fixed');
            $('#menu-lang').removeClass('langFix');
        }
    });
    //date
    $( "#datepicker" ).datepicker();
     $( "#datepicker" ).click(function(){
        $(".bg-popup").hide();
        $("#menu-lang").hide();
        $("#cat_popup").hide();
     })
    $(".fa-heart-o").click(function(){
        $(".plus").addClass("plus-heart");
    });    
    $("#popup-click-show").click(function(){
//        $(".bg-popup").show();
        $("#popup-home-page").fadeIn(300);
        $("#menu-lang").hide();
        $("#cat_popup").hide();
    });
    //smoothly
    $('.main-menu li.schedule a').bind('click',function(event){
        var $anchor = $(this);
        $("#menu-lang").hide();
        $("#cat_popup").hide();
        
        var eleHeight = 0;
        if( !$('#header-menu').hasClass('is-fixed') ){
            eleHeight = 158;
        }else{
            eleHeight = 70;
        }
        
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - eleHeight
        }, 1000,'easeInOutExpo');
        
        event.preventDefault();
    });
    $('.main-menu li.login-menu a').bind('click',function(event){
        var $anchor = $(this);
        $("#menu-lang").hide();
        $("#cat_popup").hide();
        
        var eleHeight = 0;
        if( !$('#header-menu').hasClass('is-fixed') ){
            eleHeight = 158;
        }else{
            eleHeight = 70;
        }
            
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - eleHeight
        }, 1000,'easeInOutExpo');
        
        event.preventDefault();
    });
    //checkbox
    $('.delitm-checkbox label').click(function(){
        $('.delitm-checkbox label span').fadeToggle(100);
    });
    var removeSuccess;
    removeSuccess = function () {
        $('.plus .wrap').removeClass('success');
    };
    $('.plus .button').click(function () {
        $('.plus .wrap').addClass('success');
            setTimeout(removeSuccess, 1500);
    });
    //selectbox
    $('#calende .select2').click(function(){
        $('li.select2-results__options').toggle(300);
    });
    //checkboxlogin
    $('.bottom .check-login').click(function(){
       $('.bottom .check-login label i').fadeToggle(100);
    });
    $('#form-register > a > i').click(function(){
        $('#form-register').hide(300);
        $('#bg_wrap').hide();
    });
    $('#form-login > a > i').click(function(){
        $('#bg_wrap').hide(300);
        $('#form-login').hide(300);
    });
    $('#cart-item > a > i').click(function(){
        $('#cart-item').hide(300);
        $('#bg_wrap').hide(300);
    });
    $('.formRight .left > span > label').click(function(){
        $('.formRight .left > span > label > i ').fadeToggle(200);
        $('.showForm').toggle(300);
    })
    $('#main-content').click(function(){
        $("#menu-lang").hide();
        $("#cat_popup").hide();
        $('#menu-schedule').hide();
    });
    $("#bg_wrap").click(function(){
        $('#cart-item').hide(300);
        $('#form-register').hide(300);
        $('#form-login').hide(300);
        $(this).hide(300);
        $('#popup-home-page').hide(300);
    });
    if($(window).width() < 667){
            $('.main-menu li.schedule a').bind('click',function(event){
            var $anchor = $(this);
            var eleHeight = 0;
            if( !$('#header-menu').hasClass('is-fixed') ){
                eleHeight = 128;
            }else{
                eleHeight = 70;
            }

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - eleHeight
            }, 1000,'easeInOutExpo');

            event.preventDefault();
        });
        $('.main-menu li.login-menu a').bind('click',function(event){
            var $anchor = $(this);

            var eleHeight = 0;
            if( !$('#header-menu').hasClass('is-fixed') ){
                eleHeight = 128;
            }else{
                eleHeight = 68;
            }

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - eleHeight
            }, 1000,'easeInOutExpo');

            event.preventDefault();
        });
    }
    if($(window).width() < 480){
            $('.main-menu li.schedule a').bind('click',function(event){
            var $anchor = $(this);
            var eleHeight = 0;
            if( !$('#header-menu').hasClass('is-fixed') ){
                eleHeight = 103;
            }else{
                eleHeight = 55;
            }

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - eleHeight
            }, 1000,'easeInOutExpo');

            event.preventDefault();
        });
        $('.main-menu li.login-menu a').bind('click',function(event){
            var $anchor = $(this);

            var eleHeight = 0;
            if( !$('#header-menu').hasClass('is-fixed') ){
                eleHeight = 103;
            }else{
                eleHeight = 55;
            }

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - eleHeight
            }, 1000,'easeInOutExpo');

            event.preventDefault();
        });
    }
    //SUBSCRIBE
    var addSubscribe;
    addSubscribe = function () {
        $('.wrapSub').addClass('subScrice');
    };
    $( window ).load(function() {
        setTimeout(addSubscribe, 3000);
    });
    $('.wrapSub').click(function(){
        $('.wrapSub').hide();
    }); 
});
